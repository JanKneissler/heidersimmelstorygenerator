/*******************************************************************************
 *     DiskWorld - a simple 2D physics simulation environment, 
 *     Copyright (C) 2014  Jan Kneissler
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program in the file "License.txt".  
 *     If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package diskworld.extension;

import diskworld.DiskComplex;
import diskworld.environment.AgentMapping;
import storygenerator.Assignment;

/**
 * Provides the interface to a controller that controls an agent. Sensor and actuator values are passed to and from the controller
 * in form of double arrays. This class maps the values to the corresponding actions and reads from the corresponding sensors.
 * For debugging purposes, each of the IO channels can be equipped with a name (a string).
 * 
 * @author Jan
 */
public class AgentMappingWithAssignment extends AgentMapping {

	private Assignment assignment;

	//@P
	public AgentMappingWithAssignment(DiskComplex diskComplex, Assignment assignment) {
		super(diskComplex);
		addAssignment(diskComplex, assignment);
	}

	//@P
	private void addAssignment(DiskComplex diskComplex, Assignment assignment) {
		this.assignment = assignment;
	}

	public Assignment getAssignment() {
		return assignment;
	}

}
