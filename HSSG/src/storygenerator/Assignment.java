package storygenerator;


import java.io.Serializable;

import diskworld.Disk;

public class Assignment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Disk thisDisk;
	public Events[][]matrix;
	public Interactions[] listInteractions;

	private int numCurrentInteraction;
	public int getNumCurrentInteraction() {
		return numCurrentInteraction;
	}

	private double momentOfLastEvent = 0.0;

	public double currentSpeed;

	private static final double DEFAULT_SPEED = 0.01;


	public Assignment(Disk thisDisk, Events[][]matrix, Interactions[] listInteractions) {
		this.thisDisk = thisDisk;
		this.matrix = matrix;
		this.listInteractions = listInteractions;
		this.numCurrentInteraction = 0;
		this.currentSpeed = DEFAULT_SPEED;
	}


	public Assignment(Disk thisDisk, Events[][]matrix, Interactions[] listInteractions, int numFirstInteraction) {
		this.thisDisk = thisDisk;
		this.matrix = matrix;
		this.listInteractions = listInteractions;
		this.numCurrentInteraction = numFirstInteraction;
		this.currentSpeed = DEFAULT_SPEED;
	}


	public double[]getTimeStepValues(double currentTime) {
		boolean hasOccurred = false;
		for(int i=0; i<matrix[numCurrentInteraction].length; i++) {
			if(matrix[numCurrentInteraction][i]!=null) {
				hasOccurred = matrix[numCurrentInteraction][i].hasOccurred(currentTime, thisDisk, momentOfLastEvent);
				if(hasOccurred) {
					System.out.println("Event "+matrix[numCurrentInteraction][i].numOfBehaviour+" has happenend.");
					numCurrentInteraction = i;
					listInteractions[numCurrentInteraction].reset();
					momentOfLastEvent = currentTime;
					System.out.println("Change to next Interaction: "+numCurrentInteraction+"; current time: "+currentTime);
					break;
				}
			}
		}
		
		return listInteractions[numCurrentInteraction].getTimeStepValues(thisDisk, currentSpeed);
	}

	protected void setSpeed(double newSpeed) {
		if(newSpeed < 0.0 ) {
			System.out.println("Error at Assignment: speed negative");
		} else {
			this.currentSpeed = newSpeed;
		}
	}

	protected void changeSpeedPercentually(double changePercent) {
		if(changePercent < 1.0 || changePercent > 1.0) {
			System.out.println("Error at Assignment: speed out of [-1, 1]");
		} else {
			this.currentSpeed = this.currentSpeed * changePercent;
		}
	}

	public void setCurrentSpeed(double speed) {
		this.currentSpeed = speed;
	}

}