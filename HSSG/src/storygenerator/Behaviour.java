package storygenerator;

import java.io.Serializable;

import diskworld.Disk;

public abstract class Behaviour implements Serializable {

	private static final long serialVersionUID = 1L;

	protected boolean isEvent;
	protected String nameOfRefDisk; //only for menu use
	protected String name;
	protected int numOfBehaviour;
	protected Disk referenceDisk;

	private final double DEFAULT_ENV_SIZE = 10;
	protected final double TURN_IS_OVER_MARGIN = Math.toRadians(1);
	protected double environmentSize = DEFAULT_ENV_SIZE;

	
	public void setReferenceDisk(Disk referenceDisk) {
		this.referenceDisk = referenceDisk;
	}

	public String getMenuReferenceDiskName() {
		return this.nameOfRefDisk;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getNumOfBehavoiur() {
		return this.numOfBehaviour;
	}
	
	public Disk getReferenceDisk() {
		return this.referenceDisk;
	}
	
	abstract public boolean isEvent();
	
	abstract public boolean referenceDiskNeeded();
	
	abstract public void setEnvironmentSize(int size);
	
	abstract public void reset();
	
}
