package storygenerator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.html.parser.Element;

import org.w3c.dom.Document;

import diskworld.Disk;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.environment.Wall;
import diskworld.linalg2D.Line;
import diskworld.linalg2D.Point;
import diskworld.visualization.EnvironmentPanel;

public class DemoSimulator {

	static JFrame frame = new JFrame();
	static Environment env;
	static List<Disk[]> mergeList = new LinkedList<Disk[]>();

	private static Assignment[] story = new Assignment[21];

	// interactions
	private static final int PHYSICALLY = 1;
	private static final int RANDOMLY = 2;
	private static final int TRAJECTORY = 3;
	private static final int ROTATE = 4;
	private static final int STOP = 5;
	private static final int CHASE = 6;
	private static final int AVOID = 7;
	private static final int TURN_T0 = 8;
	private static final int TRAJ_CONTINUOUSLY = 9;
	private static final int ROTATE_AROUND = 10;
	private static final int DISAPPEAR = 11;

	//events
	private static final int ALWAYS = 1;
	private static final int SEC_SINCE_START = 2;
	private static final int PERIOD_OF_TIME = 3;
	private static final int TOUCH_OBJECT = 4;
	private static final int COLLISON = 5;
	private static final int SAME_X_POS = 6;
	private static final int SAME_Y_POS = 7;
	private static final int TURN_IS_OVER = 8;
	private static final int BECAME_VISIBLE = 9;
	private static final int BECAME_INVISIBLE = 10;

	public static void main(String[] args) {

		env = new Environment(10, 10, 0.1, getWalls(false, 0));
//				avoidMany_demo();


//				follow_demo();
		//		rectangle_demo();
		//		periodOfTime_demo();
		//		turnTo_demo();

		//		env = new Environment(10, 10, 0.1, getWalls(true, 1));
		//		rotate_demo();

		//		touchRotateAround_demo();
//		sameYPosition_demo();

		//		env = new Environment(10, 10, 0.1, getWalls(true, 2));
		//		trajectory_demo();

		//		visible_demo();
		//		avoid_demo();

//				singleDisk_demo();


		test("demos storygenerator", 600, 5);
		frame.dispose();
		System.exit(0);
	}

//
//	public static void follow_demo() {
//		//construct chasing disk
//		//		Color col = Color.BLUE;
//		Color col = new Color(255, 0, 0);
//		DiskMaterial mat = new DiskMaterial(1.0, 1.0, 0.01, col);
//		DiskType disktype = new DiskType(mat);
//		Disk d1 = env.newDisk(0.33, 0.32, 0.05, disktype);
//		//		d1.attachDisk(Math.toRadians(180), 0.01, disktype);
//
//		Color col2 = Color.GREEN;
//		DiskMaterial mat2 = new DiskMaterial(1.0, 1.0, 0.0, col2);
//		DiskType disktype2 = new DiskType(mat2);
//		Disk ball1 = env.newDisk(0.64, 0.90, 0.02, disktype2);
//		ball1.applyImpulse(0.00025, 0.00007);
//
//		// disk behavior
//		Behavior chaseBallForever = new Behavior(CHASE, ball1, ALWAYS);
//		Behavior movePhysicallyAloneForever = new Behavior(PHYSICALLY, ALWAYS);
//
//		Behavior[] behaviors1 = {chaseBallForever};
//		Behavior[] behaviors2 = {movePhysicallyAloneForever};
//
//		//assignments
//		story[0] = new Assignment(d1, behaviors1);
//		story[1] = new Assignment(ball1, behaviors2);
//	}
//
//	public static void avoid_demo() {
//
//		//construct chasing disk
//		Color col = Color.BLUE;
//		DiskMaterial mat = new DiskMaterial(1.0, 1.0, 0.01, col);
//		DiskType disktype = new DiskType(mat);
//
//		Color col2 = Color.GREEN;
//		DiskMaterial mat2 = new DiskMaterial(1.0, 1.0, 0.01, col2);
//		DiskType disktype2 = new DiskType(mat2);
//		Disk ball1 = env.newDisk(0.1, 0.2, 0.03, disktype2);
//		ball1.applyImpulse(0.0003, 0.001);
//
//		Behavior avoidBall4Ever = new Behavior(AVOID, ball1, ALWAYS);
//		Behavior[] behaviors1 = {avoidBall4Ever};
//
//		story[1] = new Assignment(env.newDisk(0.8, 0.2, 0.05, disktype), behaviors1);
//		//		story[2] = new Assignment(env.newDisk(0.6, 0.2, 0.05, disktype), behaviors1);
//		//		story[3] = new Assignment(env.newDisk(0.8, 0.4, 0.05, disktype), behaviors1);
//
//
//		// disk behavior
//		Behavior movePhysicallyAloneForever = new Behavior(PHYSICALLY, ALWAYS);
//
//		Behavior[] behaviors2 = {movePhysicallyAloneForever};
//
//		//assignments
//		story[0] = new Assignment(ball1, behaviors2);
//	}
//
//	public static void avoidMany_demo() {
//
//		//construct chasing disk
//		Color col = Color.BLUE;
//		DiskMaterial mat = new DiskMaterial(1.0, 1.0, 0.01, col);
//		DiskType disktype = new DiskType(mat);
//
//
//		Color col2 = Color.GREEN;
//		DiskMaterial mat2 = new DiskMaterial(1.0, 1.0, 0.01, col2);
//		DiskType disktype2 = new DiskType(mat2);
//		Disk ball1 = env.newDisk(0.1, 0.2, 0.03, disktype2);
//		ball1.applyImpulse(0.0001, 0.0001);
//
//		Behavior avoidBall4Ever = new Behavior(AVOID, ball1, ALWAYS);
//		Behavior[] behaviors1 = {avoidBall4Ever};
//
//		for(int i=0; i<20; i++) {
//			double x = Math.random();
//			double y = Math.random();
//			story[i] = new Assignment(env.newDisk(x, y, 0.005, disktype), behaviors1);
//		}
//
//		// disk behavior
//		Behavior movePhysicallyAloneForever = new Behavior(PHYSICALLY, ALWAYS);
//
//		Behavior[] behaviors2 = {movePhysicallyAloneForever};
//
//		//assignments
//		story[20] = new Assignment(ball1, behaviors2);
//	}
//
//	public static void rectangle_demo() {
//
//		//construct disk
//		Color col = Color.BLUE;
//		DiskMaterial mat = new DiskMaterial(1.0, 1.0, 0.01, col);
//		DiskType disktype = new DiskType(mat);
//		Disk d1 = env.newDisk(0.5, 0.5, 0.05, disktype);
//
//		Point[] trajectory = {new Point(0.2, 0.2), new Point(0.8, 0.2), new Point(0.8, 0.8), new Point(0.2, 0.8)};
//
//		// disk behavior
//		Behavior moveAlongRectanlge = new Behavior(new Interactions(TRAJ_CONTINUOUSLY, trajectory), ALWAYS);
//		Behavior[] behaviors1 = {moveAlongRectanlge};
//
//		//assignments
//		story[0] = new Assignment(d1, behaviors1);
//	}
//
//	public static void rotate_demo() {
//
//		Color col = Color.BLUE;
//		DiskMaterial mat = new DiskMaterial(1.0, 1.0, 0.01, col);
//		Sensor sensor1 = new DiskDistanceSensor(env, Math.toRadians(0), Math.toRadians(30), 0.3);
//		DiskType agentSensor = new DiskType(mat, new Sensor[] { sensor1 });
//		Disk d1 = env.newDisk(0.11, 0.22, 0.05, agentSensor);
//
//		Behavior rotate = new Behavior(new Interactions(ROTATE, new Point(0.5, 0.5), 0.4), ALWAYS);
//		Behavior[] behaviors1 = {rotate};
//
//		story[0] = new Assignment(d1, behaviors1);
//	}
//
//	public static void trajectory_demo() {
//
//		//construct disk
//		Color col = Color.BLUE;
//		DiskMaterial mat = new DiskMaterial(1.0, 1.0, 0.01, col);
//		//		DiskType disktype = new DiskType(mat);
//
//		Sensor sensor1 = new DiskDistanceSensor(env, Math.toRadians(0), Math.toRadians(30), 0.3);
//		DiskType agentSensor = new DiskType(mat, new Sensor[] { sensor1 });
//
//		Disk d1 = env.newDisk(0.1, 0.9, 0.05, agentSensor);
//
//		Point[] trajectory = {new Point(0.1, 0.9), new Point(0.1, 0.1), new Point(0.4, 0.1), new Point(0.65, 0.6), new Point(0.9, 0.6), new Point(0.9, 0.9) };
//
//		// disk behavior
//		Behavior moveAlongRectangle = new Behavior(new Interactions(TRAJ_CONTINUOUSLY, trajectory), ALWAYS);
//		Behavior[] behaviors1 = {moveAlongRectangle};
//
//		//assignments
//		story[0] = new Assignment(d1, behaviors1, true);
//	}
//
//	public static void sameYPosition_demo() {
//
//		//construct disk
//		Color col = Color.BLUE;
//		DiskMaterial mat = new DiskMaterial(1.0, 1.0, 0.01, col);
//		DiskType disktype = new DiskType(mat);
//		Disk d1 = env.newDisk(0.9, 0.8, 0.03, disktype);
//		d1.applyImpulse(-0.0001, -0.0001);
//
//		Color col2 = Color.GREEN;
//		DiskMaterial mat2 = new DiskMaterial(1.0, 1.0, 0.01, col2);
//		DiskType disktype2 = new DiskType(mat2);
//		Disk d2 = env.newDisk(0.1, 0.2, 0.03, disktype2);
//		d2.applyImpulse(0.0001, 0.0001);
//
//		Behavior rotate = new Behavior(new Interactions(ROTATE, new Point(0.5, 0.5), 0.1), ALWAYS);
//		Behavior movePhyUntilBallSameHeight = new Behavior(PHYSICALLY, d2, SAME_Y_POS);
//		Behavior movePhyUntilDiskSameHeight = new Behavior(PHYSICALLY, d1, SAME_Y_POS);
//		// disk behavior
//		Behavior[] behaviors1 = {movePhyUntilBallSameHeight, rotate};
//		Behavior[] behaviors2 = {movePhyUntilDiskSameHeight, rotate};
//
//		//assignments
//		story[0] = new Assignment(d1, behaviors1);
//		story[1] = new Assignment(d2, behaviors2);
//	}
//
//	public static void branch_demo() {
//
//		//construct disks
//		Color col = Color.BLUE;
//		DiskMaterial mat = new DiskMaterial(0.0, 0.0, 0.0, 0.0, col);
//		DiskType disktype = new DiskType(mat);
//		Disk d1 = env.newRootDisk(0.8, 0.9, 0.03, disktype);
////		Disk d1 = env.newRootDisk(0.9, 0.8, 0.03, disktype);
//		d1.applyImpulse(-0.0001, -0.0001);
//
//		Color col2 = Color.GREEN;
//		DiskMaterial mat2 = new DiskMaterial(0.0, 0.0, 0.0, 0.0, col2);
//		DiskType disktype2 = new DiskType(mat2);
//		Disk d2 = env.newRootDisk(0.2, 0.1, 0.03, disktype2);
////		Disk d2 = env.newRootDisk(0.1, 0.2, 0.03, disktype2);
//		d2.applyImpulse(0.0001, 0.0001);
//		
//		//interactions d1
//		int numInteractionsD1 = 3;
//		Interactions movePhy = new Interactions(PHYSICALLY, 0); //0
//		Interactions rotate = new Interactions(ROTATE, new Point(0.5, 0.5), 0.1, 1);//1
//		Interactions stop = new Interactions(STOP, 2);//2
//		Interactions[] listIAofD1 = {movePhy, rotate, stop};
//
//		
//		//events d1
//		Events sameXPosWithD2 = new Events(SAME_X_POS, d2);
//		Events sameYPosWithD2 = new Events(SAME_Y_POS, d2);
//		Events collision = new Events(COLLISON);
//		Events always = new Events(ALWAYS);
//		
//		Events[][] adjMatrixD1 = getAdjMatrix(numInteractionsD1);
//		adjMatrixD1[movePhy.serialNum][stop.serialNum] = sameXPosWithD2; //TODO: write method for all those assignments params: (movePhy, stop, sameX);
//		adjMatrixD1[stop.serialNum][stop.serialNum] = always;
//		adjMatrixD1[movePhy.serialNum][rotate.serialNum] = sameYPosWithD2;
//		adjMatrixD1[rotate.serialNum][rotate.serialNum] = always;
//		
//		Assignment assignmD1 = new Assignment(d1, adjMatrixD1, listIAofD1);
//	
//		
//		//interactions d2
//		int numInteractionsD2 = 3;
//		Interactions movePhyd2 = new Interactions(PHYSICALLY, 0); //0
//		Interactions rotated2 = new Interactions(ROTATE, new Point(0.5, 0.5), 0.1, 1);//1
//		Interactions randomd2 = new Interactions(RANDOMLY, 2);//2
//		Interactions[] listIAofD2 = {movePhyd2, rotated2, randomd2};
//
//		//events d1
//		Events sameXPosWithD1 = new Events(SAME_X_POS, d1);
//		Events sameYPosWithD1 = new Events(SAME_Y_POS, d1);
//		Events alwaysd2 = new Events(ALWAYS);
//		
//		Events[][] adjMatrixD2 = getAdjMatrix(numInteractionsD2);
//		adjMatrixD2[movePhyd2.serialNum][randomd2.serialNum] = sameXPosWithD1;
//		adjMatrixD2[randomd2.serialNum][randomd2.serialNum] = alwaysd2;
//		adjMatrixD2[movePhyd2.serialNum][rotated2.serialNum] = sameYPosWithD1;
//		adjMatrixD2[rotated2.serialNum][rotated2.serialNum] = alwaysd2;
//				
//		Assignment assignmD2 = new Assignment(d2, adjMatrixD2, listIAofD2);
//		
//		story[0] = assignmD1;
//		story[1] = assignmD2;
//		
////		for(int i=0; i<adjMatrixD1.length; i++) {
////			for(int j=0; j<adjMatrixD1.length; j++) {
////				int s = (adjMatrixD1[i][j]!=null)?adjMatrixD1[i][j].numTypeOfEvent:9;
////				System.out.print("  "+s);
////			}
////			System.out.println();
////		}
//	}
//	
//	public static void branchXML_() {
//		
//
//		//construct disks
//		Color col = Color.BLUE;
//		DiskMaterial mat = new DiskMaterial(1.0, 1.0, 0.01, 0.0, col);
//		DiskType disktype = new DiskType(mat);
////		Disk d1 = env.newDisk(0.8, 0.9, 0.03, disktype);
//		Disk d1 = env.newRootDisk(0.9, 0.8, 0.03, disktype);
//		d1.applyImpulse(-0.0001, -0.0001);
//
//		Color col2 = Color.GREEN;
//		DiskMaterial mat2 = new DiskMaterial(1.0, 1.0, 0.01, 0.0, col2);
//		DiskType disktype2 = new DiskType(mat2);
////		Disk d2 = env.newDisk(0.2, 0.1, 0.03, disktype2);
//		Disk d2 = env.newRootDisk(0.1, 0.2, 0.03, disktype2);
//		d2.applyImpulse(0.0001, 0.0001);
//		
//		//interactions d1
//		int numInteractionsD1 = 3;
//		Interactions movePhy = new Interactions(PHYSICALLY, 0); //0
//		Interactions rotate = new Interactions(ROTATE, new Point(0.5, 0.5), 0.1, 1);//1
//		Interactions stop = new Interactions(STOP, 2);//2
//		Interactions[] listIAofD1 = {movePhy, rotate, stop};
//
//		
//		//events d1
//		Events sameXPosWithD2 = new Events(SAME_X_POS, d2);
//		Events sameYPosWithD2 = new Events(SAME_Y_POS, d2);
//		Events collision = new Events(COLLISON);
//		Events always = new Events(ALWAYS);
//		
//		Events[][] adjMatrixD1 = getAdjMatrix(numInteractionsD1);
//		adjMatrixD1[movePhy.serialNum][stop.serialNum] = sameXPosWithD2; //TODO: write method for all those assignments params: (movePhy, stop, sameX);
//		adjMatrixD1[stop.serialNum][stop.serialNum] = always;
//		adjMatrixD1[movePhy.serialNum][rotate.serialNum] = sameYPosWithD2;
//		adjMatrixD1[rotate.serialNum][rotate.serialNum] = always;
//		
//		Assignment assignmD1 = new Assignment(d1, adjMatrixD1, listIAofD1);
//	
//		
//		//interactions d2
//		int numInteractionsD2 = 3;
//		Interactions movePhyd2 = new Interactions(PHYSICALLY, 0); //0
//		Interactions rotated2 = new Interactions(ROTATE, new Point(0.5, 0.5), 0.1, 1);//1
//		Interactions randomd2 = new Interactions(RANDOMLY, 2);//2
//		Interactions[] listIAofD2 = {movePhyd2, rotated2, randomd2};
//
//		//events d1
//		Events sameXPosWithD1 = new Events(SAME_X_POS, d1);
//		Events sameYPosWithD1 = new Events(SAME_Y_POS, d1);
//		Events alwaysd2 = new Events(ALWAYS);
//		
//		Events[][] adjMatrixD2 = getAdjMatrix(numInteractionsD2);
//		adjMatrixD2[movePhyd2.serialNum][randomd2.serialNum] = sameXPosWithD1;
//		adjMatrixD2[randomd2.serialNum][randomd2.serialNum] = alwaysd2;
//		adjMatrixD2[movePhyd2.serialNum][rotated2.serialNum] = sameYPosWithD1;
//		adjMatrixD2[rotated2.serialNum][rotated2.serialNum] = alwaysd2;
//				
//		Assignment assignmD2 = new Assignment(d2, adjMatrixD2, listIAofD2);
//		
//		story[0] = assignmD1;
//		story[1] = assignmD2;
//		
//	}
//	
//	public static void singleDisk_demo() {
//
////		//construct disks
////		Color col = Color.BLUE;
////		DiskMaterial mat = new DiskMaterial(1.0, 1.0, 0.01, col);
////		DiskType disktype = new DiskType(mat);
////		Disk d1 = env.newDisk(0.9, 0.8, 0.03, disktype);
////		d1.applyImpulse(-0.0001, -0.0001);
//
//		Color col2 = Color.GREEN;
//		DiskMaterial mat2 = new DiskMaterial(1.0, 1.0, 0.01, 0.0, col2);
//		DiskType disktype2 = new DiskType(mat2);
//		Disk d2 = env.newRootDisk(0.1, 0.2, 0.03, disktype2);
//		d2.applyImpulse(0.0001, 0.0001);
//		
////		//interactions d1
////		int numInteractionsD1 = 3;
////		Interactions movePhy = new Interactions(PHYSICALLY, 0); //0
////		Interactions rotate = new Interactions(ROTATE, new Point(0.5, 0.5), 0.1, 1);//1
////		Interactions stop = new Interactions(STOP, 2);//2
////		Interactions[] listIAofD1 = {movePhy, rotate, stop};
////
////		
////		//events d1
////		Events sameXPosWithD2 = new Events(SAME_X_POS, d2);
////		Events sameYPosWithD2 = new Events(SAME_Y_POS, d2);
//		Events collision = new Events(COLLISON);
////		Events always = new Events(ALWAYS);
////		
////		Events[][] adjMatrixD1 = new Events[numInteractionsD1][numInteractionsD1];
////		adjMatrixD1[movePhy.serialNum][stop.serialNum] = collision; //TODO: write method for all those assignments params: (movePhy, stop, sameX);
////		adjMatrixD1[stop.serialNum][stop.serialNum] = always;
////		adjMatrixD1[movePhy.serialNum][rotate.serialNum] = collision;
////		adjMatrixD1[rotate.serialNum][rotate.serialNum] = always;
////		
////		Assignment assignmD1 = new Assignment(d1, adjMatrixD1, listIAofD1);
//	
//		
//		//interactions d2
//		int numInteractionsD2 = 3;
//		Interactions movePhyd2 = new Interactions(PHYSICALLY, 0); //0
//		Interactions rotated2 = new Interactions(ROTATE, new Point(0.5, 0.5), 0.1, 1);//1
//		Interactions randomd2 = new Interactions(RANDOMLY, 2);//2
//		Interactions[] listIAofD2 = {movePhyd2, rotated2, randomd2};
//
////		//events d1
////		Events sameXPosWithD1 = new Events(SAME_X_POS, d1);
////		Events sameYPosWithD1 = new Events(SAME_Y_POS, d1);
//		Events alwaysd2 = new Events(ALWAYS);
////		
//		Events[][] adjMatrixD2 = new Events[numInteractionsD2][numInteractionsD2];
//		adjMatrixD2[movePhyd2.serialNum][randomd2.serialNum] = collision;
//		adjMatrixD2[randomd2.serialNum][randomd2.serialNum] = alwaysd2;
//		adjMatrixD2[movePhyd2.serialNum][rotated2.serialNum] = collision;
//		adjMatrixD2[rotated2.serialNum][rotated2.serialNum] = alwaysd2;
//				
//		Assignment assignmD2 = new Assignment(d2, adjMatrixD2, listIAofD2);
//		
////		story[0] = assignmD1;
//		story[0] = assignmD2;
//		
////		for(int i=0; i<adjMatrixD1.length; i++) {
////			for(int j=0; j<adjMatrixD1.length; j++) {
////				int s = (adjMatrixD1[i][j]!=null)?adjMatrixD1[i][j].numTypeOfEvent:9;
////				System.out.print("  "+s);
////			}
////			System.out.println();
////		}
//	}

//	public static void touchRotateAround_demo() {
//
//		//construct disk
//		Color col = Color.BLUE;
//		DiskMaterial mat = new DiskMaterial(1.0, 1.0, 0.01, col);
//		DiskType disktype = new DiskType(mat);
//		Disk d1 = env.newDisk(0.9, 0.9, 0.03, disktype);
//		d1.applyImpulse(-0.0001, -0.0001);
//
//		Color col2 = Color.GREEN;
//		DiskMaterial mat2 = new DiskMaterial(1.0, 1.0, 0.01, col2);
//		DiskType disktype2 = new DiskType(mat2);
//		Disk d2 = env.newDisk(0.1, 0.1, 0.03, disktype2);
//		d2.applyImpulse(0.0001, 0.0001);
//
//		Behavior rotateAround = new Behavior(new Interactions(ROTATE_AROUND, 0.1), d1, ALWAYS);
//		Behavior movePhyUntilTouch1 = new Behavior(PHYSICALLY, d2, TOUCH_OBJECT);
//		Behavior movePhyUntilTouch2 = new Behavior(PHYSICALLY, d1, TOUCH_OBJECT);
//		Behavior stopForever = new Behavior(STOP, ALWAYS);
//		// disk behavior
//		Behavior[] behaviors1 = {movePhyUntilTouch1, stopForever};
//		Behavior[] behaviors2 = {movePhyUntilTouch2, rotateAround};
//
//		//assignments
//		story[0] = new Assignment(d1, behaviors1);
//		story[1] = new Assignment(d2, behaviors2);
//	}
//
//	public static void periodOfTime_demo() {
//		//construct disk
//		Color col = Color.BLUE;
//		DiskMaterial mat = new DiskMaterial(1.0, 1.0, 0.01, col);
//		DiskType disktype = new DiskType(mat);
//		Color col2 = Color.ORANGE;
//		DiskMaterial mat2 = new DiskMaterial(1.0, 1.0, 0.01, col2);
//		DiskType disktype2 = new DiskType(mat2);
//		Disk d1 = env.newDisk(0.2, 0.8, 0.05, disktype);
//		Disk d2 = env.newDisk(0.8, 0.2, 0.05, disktype2);
//
//		d1.applyImpulse(-0.001, 0.0);
//		d2.applyImpulse(0.001, 0.0);
//
//		Behavior movePhysicallyUntilSameXPosWithD2 = new Behavior(PHYSICALLY, d2, SAME_X_POS);
//		Behavior stopUntil5sPassed = new Behavior(STOP, new Events(PERIOD_OF_TIME, 5.0));
//
//		Behavior movePhysically = new Behavior(PHYSICALLY, ALWAYS);
//
//		Behavior[] b1 = {movePhysicallyUntilSameXPosWithD2, stopUntil5sPassed};
//		Behavior[] b2 = {movePhysically};
//
//		story[0] = new Assignment(d1, b1, true);
//		story[1] = new Assignment(d2, b2);
//	}
//
//	public static void visible_demo() {
//		DiskType disktype = new DiskType(DiskMaterial.RUBBER);
//		Set<DiskMaterial> visibleMaterials = new HashSet<DiskMaterial>();
//		visibleMaterials.add(DiskMaterial.RUBBER);
//		Sensor sensor1 = new ColorSensitiveDirectionSensor(env, Math.toRadians(90), Math.toRadians(60), 0.8, visibleMaterials);
//		DiskType agentSensor = new DiskType(DiskMaterial.METAL, new Sensor[] { sensor1 });
//
//		//construct disks
//		Disk d1 = env.newDisk(0.2, 0.8, 0.05, disktype);
//		Disk d2 = env.newDisk(0.8, 0.2, 0.05, agentSensor);
//
//		d1.applyImpulse(0.0005, 0.0);
//		d2.applyImpulse(-0.0005, 0.0);
//
//		Behavior movePhysicallyForever = new Behavior(PHYSICALLY, ALWAYS);
//		Behavior stopUntilVisible = new Behavior(STOP, new Events(BECAME_VISIBLE, sensor1));
//		Behavior movePhysicallyUntilInvisible = new Behavior(PHYSICALLY, new Events(BECAME_INVISIBLE, sensor1));
//
//		Behavior[] b1 = {movePhysicallyForever};
//		Behavior[] b2 = {stopUntilVisible, movePhysicallyUntilInvisible};
//
//		story[0] = new Assignment(d1, b1);
//		story[1] = new Assignment(d2, b2, true);
//	}
//
//	public static void turnTo_demo() {
//		DiskType disktype = new DiskType(DiskMaterial.RUBBER);
//		Set<DiskMaterial> visibleMaterials = new HashSet<DiskMaterial>();
//		visibleMaterials.add(DiskMaterial.RUBBER);
//		Sensor sensor1 = new DiskDistanceSensor(env, Math.toRadians(0), Math.toRadians(30), 0.3);
//		DiskType agentSensor = new DiskType(DiskMaterial.METAL, new Sensor[] { sensor1 });
//
//		Disk d1 = env.newDisk(0.2, 0.8, 0.05, agentSensor);
//		Disk d2 = env.newDisk(0.8, 0.2, 0.05, disktype);
//
//		d1.applyImpulse(-0.001, 0.0);
//
//		Behavior movePhysicallyUntilSameXPosWithD2 = new Behavior(PHYSICALLY, d2, SAME_X_POS);
//		Behavior stopForever = new Behavior(STOP, ALWAYS);
//		Behavior turnToOtherDisk = new Behavior(TURN_T0, d2, TURN_IS_OVER);
//		Behavior stopFor5s = new Behavior(STOP, new Events(PERIOD_OF_TIME, 5.0));
//		Behavior chaseForever = new Behavior(CHASE, d2, ALWAYS);
//
//		Behavior[] b1 = {movePhysicallyUntilSameXPosWithD2, turnToOtherDisk, stopFor5s, chaseForever};
//		Behavior[] b2 = {stopForever};
//
//		story[0] = new Assignment(d1, b1);
//		story[1] = new Assignment(d2, b2);
//	}

	public static void test(String title, int numsteps, int sleepTime) {
		System.out.println(title);
		env.setTime(0);
		EnvironmentPanel ep = new EnvironmentPanel();
		ep.setEnvironment(env);
		//		ep.getSettings().setCircleSymbol(2, 0.5);
		//ep.getSettings().setViewedRect(0.0,0.0,0.5,0.5);
		JPanel panel = new JPanel();
		frame.add(panel);
		frame.setTitle(title);
		frame.setVisible(true);
		frame.setSize(new Dimension(620, 700)); //620,700
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel label = new JLabel(title);
		label.setFont(new Font(Font.SERIF, Font.BOLD, 36));
		panel.add(label);
		ep.setPreferredSize(new Dimension(600, 600)); //600,600
		panel.add(ep);
		frame.add(panel);
		frame.validate();
		final char[] key = new char[1];
		frame.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				key[0] = e.getKeyChar();
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
			}
		});
		//		try {
		//			Thread.sleep(1000);
		//		} catch (InterruptedException e) {
		//		}
		do {
			key[0] = 0;
			//		for (int i = 0; i < numsteps; i++) {
			long ts = System.currentTimeMillis();
			double dt = 0.01;
			//			env.doTimeStep(dt);
//			env.doTimeStep(story, env.getTime(), dt);//TODO: implement timeStep new after pull
			env.setTime(env.getTime() + dt);
			synchronized (env) {
				for (Disk merge[] : mergeList) {
					env.getDiskComplexesEnsemble().mergeDiskComplexes(merge[0], merge[1]);
				}
			}
			mergeList.clear();
			ep.repaint();
			long time = System.currentTimeMillis() - ts;
			long sleep = sleepTime - time;
			if (sleep > 0) {
				try {
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
				}
			}
			//	}
			//	while (key[0] == 0) {
			//		try {
			//			Thread.sleep(100);
			//		} catch (InterruptedException e1) {
			//		}
			//	}
		} while (key[0] != ' ');
		panel.remove(ep);
		//env.clear();
		//		try {
		//		Thread.sleep(1000);
		//} catch (InterruptedException e) {
		//}
	}
	
	private static Events[][]getAdjMatrix(int numInteractions) {
		return new Events[numInteractions][numInteractions];
	}

	public static LinkedList<Wall> getWalls(boolean insidewall, int numWalls) {
		LinkedList<Wall> walls = new LinkedList<Wall>();
		walls.add(new Wall(new Line(new Point(0, 0), new Point(1, 0)), 0.01)); // these 4 walls are the outside walls of the frame
		walls.add(new Wall(new Line(new Point(1, 0), new Point(1, 1)), 0.01));
		walls.add(new Wall(new Line(new Point(1, 1), new Point(0, 1)), 0.01));
		walls.add(new Wall(new Line(new Point(0, 1), new Point(0, 0)), 0.01));
		if (insidewall)
			switch(numWalls) {
			case 1:
				walls.add(new Wall(new Line(new Point(0.49, 0.5), new Point(0.51, 0.5)), 0.6));
				break;
			case 2:
				walls.add(new Wall(new Line(new Point(0.25, 0.25), new Point(0.25, 0.75)), 0.1));
				walls.add(new Wall(new Line(new Point(0.25, 0.75), new Point(0.75, 0.75)), 0.1));
				walls.add(new Wall(new Line(new Point(0.75, 0.0), new Point(0.85, 0.25)), 0.5));
				break;
			case 3:
				walls.add(new Wall(new Line(new Point(0.0, 0.5), new Point(0.4, 0.5)), 0.1));
				walls.add(new Wall(new Line(new Point(1.0, 0.5), new Point(0.6, 0.5)), 0.1));
				break;
			}
		return walls;
	}

}
