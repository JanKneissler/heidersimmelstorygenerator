package storygenerator;

import java.util.LinkedList;

import storygenerator.menu.A_MenuMain;
import storygenerator.menu.MenuAssignment;
import storygenerator.menu.MenuDisk;
import diskworld.Disk;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.actuators.Teleporter;
import diskworld.environment.FloorCellType;
import diskworld.linalg2D.Line;
import diskworld.linalg2D.Point;

/**
 * providing a test story for the menu
 * @author robert
 *
 */
public class SampleStoryForMenuUse {
	
	
	/**
	 * method returns a list of Assignments that show a small story
	 * -> designed as a method to test whether features and initialization
	 * are working seamlessly given this storyline.
	 * @return
	 */
	public static Story getSampleStoryUsingOnlyMenuDisks() {

		int PHYSICALLY = 1;
		int ROTATE = 4;
		int ROTATE_AROUND = 10;

		int ALWAYS = 1;
		int SEC_SINCE_START = 2;

		int NUM_AGENTS = 4;
		LinkedList<MenuAssignment> storyAssignments = new LinkedList<MenuAssignment>();
		int sizex = 10;
		int sizey = 10;
		Environment env = new Environment(sizex, sizey);
		env.getFloor().fill(FloorCellType.EMPTY);

		Teleporter myTeleporter = new Teleporter(env, false, 0.5, 0.0, 0.0, 1.0);

		DiskType agentDrive = new DiskType(DiskMaterial.METAL.withColor(A_MenuMain.DEFAULT_DISK_COLOR), myTeleporter);

		Disk d1 = env.newRootDisk(0.2, 0.2, 0.02, 0, agentDrive);
		MenuDisk DiskOne = new MenuDisk(d1);
		DiskOne.diskName="Center Disk";

		Disk d2= env.newRootDisk(0.8, 0.8, 0.02, 0, agentDrive);
		MenuDisk DiskTwo = new MenuDisk(d2);
		DiskTwo.diskName="Rotate Disk 1";

		Disk d3= env.newRootDisk(0.8, 0.2, 0.02, 0, agentDrive);
		MenuDisk DiskThree = new MenuDisk(d3);
		DiskThree.diskName="Rotate Disk 2";

		//		DiskOne.applyImpulse(0.1, 0.1);
		//		DiskTwo.applyImpulse(-0.1, -0.21);
		//		DiskThree.applyImpulse(0.1, -0.08);

		//		Events collision = new Events(COLLISON);
		Events collision = new Events(SEC_SINCE_START, 3.0);
		Events always = new Events(ALWAYS);

		int numIADiskOne = 2;
		Interactions phyDiskOne = new Interactions(PHYSICALLY);
		Interactions rotateDiskOne = new Interactions(ROTATE, new Point(0.5, 0.5), 0.1);
		//		Interactions rotateDiskOne = new Interactions(RANDOMLY);
		Interactions[] listIADiskOne = {phyDiskOne, rotateDiskOne};
		Events[][] adjMatrixDiskOne = new Events[numIADiskOne][numIADiskOne];
		adjMatrixDiskOne[0][1] = collision;
		adjMatrixDiskOne[1][1] = always;
		MenuAssignment assyDiskOne = new MenuAssignment(DiskOne, adjMatrixDiskOne, listIADiskOne);
		storyAssignments.add(assyDiskOne);

		int numIADiskTwo = 2;
		Interactions phyDiskTwo = new Interactions(PHYSICALLY);
		//		Interactions rotateDiskTwo = new Interactions(ROTATE, new Point(5, 5), 1);
		Interactions rotateDiskTwo = new Interactions(ROTATE_AROUND, DiskOne, 0.2);
		//		Interactions rotateDiskTwo = new Interactions(CHASE, DiskOne);
		Interactions[] listIADiskTwo = {phyDiskTwo, rotateDiskTwo};
		Events[][] adjMatrixDiskTwo = new Events[numIADiskTwo][numIADiskTwo];
		adjMatrixDiskTwo[0][1] = collision;
		adjMatrixDiskTwo[1][1] = always;
		MenuAssignment assyDiskTwo = new MenuAssignment(DiskTwo, adjMatrixDiskTwo, listIADiskTwo);
		storyAssignments.add(assyDiskTwo);

		int numIADiskThree = 2;
		Interactions phyDiskThree = new Interactions(PHYSICALLY);
		//		Interactions rotateDiskThree = new Interactions(ROTATE, new Point(5, 5), 1);
		Interactions rotateDiskThree = new Interactions(ROTATE_AROUND, DiskTwo, 0.1);
		//		Interactions rotateDiskThree = new Interactions(CHASE, DiskOne);
		Interactions[] listIADiskThree = {phyDiskThree, rotateDiskThree};
		Events[][] adjMatrixDiskThree = new Events[numIADiskThree][numIADiskThree];
		adjMatrixDiskThree[0][1] = collision;
		adjMatrixDiskThree[1][1] = always;
		MenuAssignment assyDiskThree = new MenuAssignment(DiskThree, adjMatrixDiskThree, listIADiskThree);
		storyAssignments.add(assyDiskThree);

		for(int i=4; i<=NUM_AGENTS; i++) {
			Disk DiskND = env.newRootDisk(0.24, 0.42, 0.02, 0, agentDrive);
			DiskND.setName("Rotate Disk "+Integer.toString(i-1));
			double impulseX = 0.02;
			double impulseY = -0.01;
			DiskND.applyImpulse(impulseX, impulseY);
			int numIADiskND = 2;
			Interactions phyDiskND = new Interactions(PHYSICALLY);
			//			Interactions rotateDiskND = new Interactions(ROTATE, new Point(5, 5), 1);
			Interactions rotateDiskND = new Interactions(ROTATE_AROUND, DiskThree, 0.05);
			//			Interactions rotateDiskND = new Interactions(CHASE, DiskOne);
			Interactions[] listIADiskND = {phyDiskND, rotateDiskND};
			Events[][] adjMatrixDiskND = new Events[numIADiskND][numIADiskND];
			adjMatrixDiskND[0][1] = collision;
			adjMatrixDiskND[1][1] = always;
			MenuAssignment assyDiskND = new MenuAssignment(DiskND, adjMatrixDiskND, listIADiskND);
			storyAssignments.add(assyDiskND);
		}

		return new Story(storyAssignments, "Rotation Story", new LinkedList<Line>(), false);
	}
	
	

	
}
