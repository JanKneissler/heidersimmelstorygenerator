package storygenerator.menu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import storygenerator.Assignment;
import storygenerator.Events;
import storygenerator.Interactions;
import storygenerator.SampleStoryForMenuUse;
import storygenerator.Story;
import storygenerator.StoryReadyToPlay;
import diskworld.Environment;
import diskworld.demos.DemoLauncher.Demo;
import diskworld.environment.AgentMapping;
import diskworld.environment.FloorCellType;
import diskworld.environment.Wall;
import diskworld.extension.AgentControllerWithAssignment;
import diskworld.extension.AgentMappingWithAssignment;
import diskworld.interfaces.AgentController;
import diskworld.linalg2D.Line;

/**
 * Main class of the storygenerator menu
 * @author robert
 *
 */
public class A_MenuMain extends JFrame {

	//can be changed if desired; useful for debugging
	private static final boolean DO_ASSIGNMENTS_EXIST = false;

	//some default values
	public		static final Color 	DEFAULT_DISK_COLOR 	= Color.ORANGE;
	public 		static final String	DEFAULT_STRING		= "-----";
	public	 	static final int 	SIZE_OF_ENVIRONMENT = 10;
	private 	static final Color	BACKGROUND_COLOR = Color.white;
	private 	static final String	DEFAULT_STORYNAME 	= "HeiderSimmelStory";

	//story details
	private LinkedList<MenuAssignment> listAss = new LinkedList<MenuAssignment>();
	private LinkedList<Line> wallList = new LinkedList<Line>();
	private String storyName;
	private boolean showVisualizationOfDiskDirection;

	//for menu use
	private static 	JButton addDiskButton;
	private static 	JButton changeDiskButton;
	private static 	JButton deleteDiskButton;
	private static 	JButton showDiskDirectionButton;
	private Environment env;
	private JSplitPane splitpane;
	private JLabel storyNameLabel;
	private ChooseEvents chooserPointer;
	private JTabbedPane tabpane = new JTabbedPane();
	private LeftEnvironmentPanel leftPanelEnvironment = new LeftEnvironmentPanel();
	private LeftPaintPanel leftPaintPanel;

	//for thread safety
	private AtomicBoolean running = new AtomicBoolean(true);

	public A_MenuMain() {
		//frame
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setLocationRelativeTo(null);
		this.setTitle("Storygenerator Menu");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setBackground(BACKGROUND_COLOR);
		this.storyName = DEFAULT_STORYNAME;
		this.showVisualizationOfDiskDirection = false;

		createButtonsAndLabel();

		MenuBar bar = new MenuBar();
		this.setJMenuBar(bar);

		if(DO_ASSIGNMENTS_EXIST) {
			Story myStory = SampleStoryForMenuUse.getSampleStoryUsingOnlyMenuDisks();
			storyName = myStory.getStoryname();
			listAss = myStory.getListOfMenuAsssignments();
			roundAllValues(listAss, 2);
		}

		splitpane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		leftPaintPanel = new LeftPaintPanel(this, listAss, wallList);
		setDefaultEnvironment();

		boolean tryNew = true;	

		if(!tryNew) {
			splitpane.setLeftComponent(leftPanelEnvironment);
		} else {
			splitpane.setLeftComponent(leftPaintPanel);
		}
		//		addMouseListenerToLeftPanel();
		splitpane.setRightComponent(getRightPanel());
		this.add(splitpane);

		this.addMenuActions(bar);

		this.setVisible(true);
		this.setResizable(false);
		restoreDefaultSplitpaneDividerLocation(splitpane);
		this.validate();
	}

	private static void roundAllValues(LinkedList<MenuAssignment> listAss, int numDecimalPlaces) {
		for(MenuAssignment ma : listAss) {
			ma.roundValues(numDecimalPlaces);
		}
	}

	private int getEnvironmentPanelSize() {
		return (int) (splitpane.getSize().width /2);
	}

	private void createButtonsAndLabel() {
		addDiskButton = new JButton("Add a disk");
		changeDiskButton = new JButton("Change a disk");
		deleteDiskButton = new JButton("Delete a disk");
		showDiskDirectionButton = new JButton("Show disk direction");
		showDiskDirectionButton.setToolTipText("Use this button to show disks' heading direction.");
		storyNameLabel = new JLabel();
	}



	private JPanel getRightPanel() {
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BorderLayout());

		JPanel helperPanel = new JPanel();
		JPanel storyNamePanel = getStoryNamePanel();
		JPanel diskActionPanel = new JPanel();
		diskActionPanel.setBackground(BACKGROUND_COLOR);
		diskActionPanel.add(addDiskButton);
		diskActionPanel.add(changeDiskButton);
		diskActionPanel.add(deleteDiskButton);
		diskActionPanel.add(showDiskDirectionButton);
		helperPanel.setLayout(new BorderLayout());
		helperPanel.add(storyNamePanel, BorderLayout.CENTER);
		helperPanel.add(diskActionPanel, BorderLayout.SOUTH);
		rightPanel.add(helperPanel, BorderLayout.NORTH);

		//JTabbedPane
		tabpane = getTable();
		rightPanel.add(tabpane, BorderLayout.CENTER);

		return rightPanel;
	}

	private JPanel getStoryNamePanel() {
		JPanel panel = new JPanel();
		panel.setBackground(Color.white);
		panel.setLayout(new BorderLayout());

		storyNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		storyNameLabel.setText("The name of your story: "+storyName);
		panel.add(storyNameLabel, BorderLayout.CENTER);

		JPanel rightNestedPanel = new JPanel();
		rightNestedPanel.setLayout(new GridLayout(1,1));
		final JButton changeStoryNameButton = new JButton("Change");
		rightNestedPanel.add(changeStoryNameButton);
		panel.add(rightNestedPanel, BorderLayout.EAST);

		changeStoryNameButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed (ActionEvent e)
			{
				if(e.getSource() == changeStoryNameButton) {
					String title = "choosing a story name";
					String labeltext = "Please enter the name of your story";
					final ChooseSomeText chooser = new ChooseSomeText(storyName, title, labeltext);

					chooser.buttonOK.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed (ActionEvent e)
						{
							if(e.getSource() == chooser.buttonOK) {
								setStoryName(chooser.tf1.getText());
								chooser.frame.setVisible(false);
							}
						}
					});
				}
			}
		});
		return panel;
	}

	private void setStoryName(String newStoryname) {
		storyName = newStoryname;
		storyNameLabel.setText("The name of your story: "+storyName);
	}

	private void createNewStory() {
		for(int i=listAss.size()-1; i>=0; i--) {
			deleteDiskAndTab(i);
		}
		wallList = new LinkedList<Line>();
		setStoryName(DEFAULT_STORYNAME);
		setDefaultEnvironment();
		leftPaintPanel = new LeftPaintPanel(this, listAss, wallList);
		changeLeftPanel(true);
		this.validate();
	}

	public void changeLeftPanel(boolean showPaintPanel) {
		if(showPaintPanel) {
			splitpane.setLeftComponent(leftPaintPanel);
		} else {
			splitpane.setLeftComponent(leftPanelEnvironment);
		}
		restoreDefaultSplitpaneDividerLocation(splitpane);
	}

	private void addMenuActions(MenuBar bar) {

		bar.menuNewStory.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(null==listAss || listAss.size()==0) {
					setStoryName(DEFAULT_STORYNAME);
					new ShowErrorMessage("Done!", "New story created.");
				} else {
					String message = "<html>If you create a new story, "
							+ "the current one will be lost.<br/>Please "
							+ "press cancel and safe the story if you "
							+ "want to keep it.</html>";
					final ShowOKSureMessage oksure = new ShowOKSureMessage("Discard current story?", message);

					oksure.buttonOK.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							oksure.frame.setVisible(false);

							createNewStory();
						}
					});
				}	
			}
		});

		bar.menuLoadStory.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(null==listAss || listAss.size()==0) {
					openFileChooser();
				} else {
					String message = "<html>If you load a new story, "
							+ "the current one will be lost.<br/>Please "
							+ "press cancel and safe the story if you "
							+ "want to keep it.</html>";
					final ShowOKSureMessage oksure = new ShowOKSureMessage("Discard current story?", message);

					oksure.buttonOK.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							oksure.isChosen = true;
							oksure.frame.setVisible(false);
							openFileChooser();
						}
					});
				}	
			}
		});

		bar.menuSafeStory.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(null==listAss || listAss.size()==0) {
					new ShowErrorMessage("Nothing to safe", "There is no story "
							+ "to safe - please create disks first.");
				} else {
					FileHandler.openFileSafer(storyName, listAss, storyName, wallList, showVisualizationOfDiskDirection);
				}	
			}
		});

		bar.menuRunNewSimulation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				if(storyIsExisting()) {

					boolean interactionsAllOk = true;
					String diskList = "";
					for(MenuAssignment ass : listAss) {
						if(null==ass.listInteractions || ass.listInteractions.size()==0) {

							if(interactionsAllOk) {
								diskList += " "+ass.thisDisk.diskName;
							} else {
								diskList += ", "+ass.thisDisk.diskName;
							}
							interactionsAllOk = false;

						}
					}

					if(!interactionsAllOk) {

						new ShowErrorMessage("Add Interaction(s) first", "Please add"
								+ " at least 1 Interaction for Disk(s) "+diskList+".");

					} else {
						changeLeftPanel(false);

						if(running.get()) {
							killSimulationThread();
						}

						SwingUtilities.invokeLater(new Runnable() {

							@Override
							public void run() {
								new Thread(new Runnable() {

									@Override
									public void run() {
										printNewSimulationStarted();
										runDemo(new StoryReadyToPlay(new Story(listAss, storyName, wallList, showVisualizationOfDiskDirection)));
									}
								}) {
								}.start();								
							}
						});
					}

				} else {
					new ShowErrorMessage("Nothing to run", "There is no story "
							+ "to run - please create disks first.");
				}
			}
		});

		bar.menuPauseSimulation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(running.get()) {
					killSimulationThread();
				}
			}
		});

		bar.menuEditStory.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(leftPanelEnvironment.isShowing()) {
					changeLeftPanel(true);
				} else if(leftPaintPanel.isShowing()) {
					new ShowErrorMessage("Already edit mode", "You are already in "
							+ "the edit mode. To change to simulation mode, "
							+ "click 'Run Simulation'.");
				}
			}
		});
	}


	/**
	 * very important method. Should be called after anything in the BigTable
	 * showing all IA x Events has changed. Prevents discrepancies between
	 * the table that is shown and new information that might have been added.
	 * @param index
	 */
	private void updateTabpane(int index) {
		JPanel getPanel = getPanelForDisk(listAss.get(index));
		tabpane.setComponentAt(index, getPanel);
		tabpane.setTitleAt(index, listAss.get(index).thisDisk.diskName);
		tabpane.setBackgroundAt(index, listAss.get(index).thisDisk.col);
		restoreDefaultSplitpaneDividerLocation(splitpane);
	}

	private boolean storyIsExisting() {
		return null!=listAss && listAss.size() > 0;
	}

	/**
	 * adds new tab, then calls updateTabpane
	 * @param index
	 */
	private void getNewTabAndUpdate(int index) {
		JPanel getPanel = getPanelForDisk(listAss.get(index));
		tabpane.addTab(listAss.get(index).thisDisk.diskName, getPanel);
		tabpane.setBackgroundAt(index, listAss.get(index).thisDisk.col);

		updateTabpane(index);
		restoreDefaultSplitpaneDividerLocation(splitpane);
		tabpane.setSelectedIndex(index);
	}


	/**
	 * returns complete JTabbedPane in which every disk gets a tab.
	 * The respective tab contains the disk data (such as x,y pos and name) as well
	 * as the Event/Interactions-Table.
	 * 
	 * @param submenu
	 * @return
	 */
	private JTabbedPane getTable() {

		tabpane = new JTabbedPane (JTabbedPane.TOP,JTabbedPane.SCROLL_TAB_LAYOUT );

		for(int i=0; i<listAss.size(); i++) {
			getNewTabAndUpdate(i);
		}

		// every time the tab is changed (by the user), the current, selected
		// tab is constructed new with the new values.
		tabpane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {

				updateTabpane(tabpane.getSelectedIndex());
			}
		});


		addDiskButton.addActionListener(new ActionListener() { 
			@Override
			public void actionPerformed (ActionEvent e)
			{
				if(e.getSource() == addDiskButton) {
					leftPaintPanel.setCurrentDrawItemToDisk();
					
					final AssignMenuDisk d = new AssignMenuDisk(listAss);
					leftPaintPanel.setAssignMenuDisk(d);

					d.buttonOK.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed (ActionEvent e)
						{
							if(e.getSource() == d.buttonOK) {
								d.diskName = d.tf1.getText();

								boolean isDisknameAlreadyUsed = d.isUsedDiskName(listAss);

								if(isDisknameAlreadyUsed) {
									chooseDifferentName();

								} else {
									addDiskAndNotifyMenu(d);
									d.dispose();

									/*try
									{
										d.x = Double.parseDouble(d.tf2.getText())/100.0;
										d.y = Double.parseDouble(d.tf3.getText())/100.0;
										d.radius = Double.parseDouble(d.tf4.getText())/100.0;

										boolean wrongInputDetected = isWrongInputAndShowError(d.x, d.y, d.radius);


										if(!wrongInputDetected) {
											addDiskAndNotifyMenu(d);
											d.dispose();
										}

									} catch(NumberFormatException nfe) {
									}*/
								}
							}
							leftPaintPanel.setAssignMenuDisk(null);
							leftPaintPanel.setCurrentDrawItemToWall();
						}
					});
				}
			}
		});

		changeDiskButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed (ActionEvent e)
			{
				if(e.getSource() == changeDiskButton) {

					if(listAss.size()==0) {
						new ShowErrorMessage("Error - create disks first",
								"You need to create disks before "
										+ "changing or deleting them.");
					} else if(listAss.size()==1) { 
						//only 1 disk -> no need to
						//ask user to select a disk
						changeDisk(0);
					} else { //more than 1 disk -> select a disk

						final ChooseDisks chooser = new ChooseDisks(listAss,
								"Choose disk to change");

						if(listAss.size()>0) {

							chooser.buttonOK.addActionListener(new ActionListener() {
								@Override
								public void actionPerformed (ActionEvent e)
								{
									if(e.getSource() == chooser.buttonOK) {
										Object diskName = chooser.tablemod.getValueAt(0, 0);
										for(int i=0; i<listAss.size(); i++) {
											if(diskName.equals(listAss.get(i).thisDisk.diskName)) {

												changeDisk(i);

												chooser.frame.setVisible(false);
												break;
											}
										}
									}
								}
							});
						}
					}
				}
			}
		});

		deleteDiskButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed (ActionEvent e)
			{
				if(e.getSource() == deleteDiskButton) {

					if(listAss.size()==0) {

						new ShowErrorMessage("Error - create disks first", "You need to create disks before "
								+ "changing or deleting them.");
					} else if (listAss.size()==1) {

						final ShowOKSureMessage okSureWindow = new ShowOKSureMessage("Sure?", "Are you sure?");

						okSureWindow.buttonOK.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed (ActionEvent e)
							{
								if(e.getSource() == okSureWindow.buttonOK) {
									okSureWindow.isChosen = true;
									okSureWindow.frame.setVisible(false);

									deleteDiskAndTab(0);
								}
							}
						});	
					} else {

						final ChooseDisks chooser = new ChooseDisks(listAss,
								"Choose disk to delete");

						chooser.buttonOK.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed (ActionEvent e) {
								if(e.getSource() == chooser.buttonOK) {

									String message = "<html>If you delete this"
											+ " Disk, every Interaction and Event that "
											+ "<br/> use this Disk as reference disk will be deleted as well."
											+ "</html>";
									final ShowOKSureMessage okSureWindow = new ShowOKSureMessage("Sure?", message);

									okSureWindow.buttonOK.addActionListener(new ActionListener() {
										@Override
										public void actionPerformed (ActionEvent e)
										{
											if(e.getSource() == okSureWindow.buttonOK) {
												okSureWindow.isChosen = true;
												okSureWindow.frame.setVisible(false);


												Object diskName = chooser.tablemod.getValueAt(0, 0);
												boolean doDelete = false;
												for(int i=0; i<listAss.size(); i++) {
													if(diskName.equals(listAss.get(i).thisDisk.diskName)) {
														doDelete = true;
														chooser.frame.setVisible(false);
														deleteDiskAndTab(i);
														break;
													}
												}
												if(!doDelete) {
													System.out.println("Error at Menu-submenu-deleteButton");
												}
											}
										}
									});	
								}
							}
						});
					}
				}
			}
		});


		showDiskDirectionButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == showDiskDirectionButton) {

					showVisualizationOfDiskDirection = !showVisualizationOfDiskDirection;
					if(showVisualizationOfDiskDirection) {
						showDiskDirectionButton.setText("Do not show disk direction");
					} else {
						showDiskDirectionButton.setText("Show disk direction");
					}
				}

			}
		});

		return tabpane;
	}

	protected static boolean isWrongInputAndShowError(
			double x, double y, double radius) {
		String wrongValueList = "";
		boolean wrongInputDetected = false;
		if(x < 0.0 || x > 1.0) {
			wrongInputDetected = true;
			wrongValueList+="x coordinate ";
		}
		if(y < 0.0 || y > 1.0) {
			wrongInputDetected = true;
			wrongValueList+="y coordinate ";
		}
		if(radius < 0.0 || radius > 1.0) {
			wrongInputDetected = true;
			wrongValueList+="radius";
		}
		if(wrongInputDetected) {
			new ShowErrorMessage("Input out "
					+ "of range", ""
							+ "Please enter values"
							+ " between 0 and 100 for the"
							+ " following value(s): "+wrongValueList );
		}
		return wrongInputDetected;
	}

	protected void addDiskAndNotifyMenu(final AssignMenuDisk d) {
		MenuDisk newDisk = new MenuDisk(d.diskName, d.x, d.y, d.radius, d.col);

		MenuAssignment ma = new MenuAssignment(newDisk, new Events[0][0], new LinkedList<Interactions>());
		listAss.add(ma);

		getNewTabAndUpdate(listAss.size()-1);
	}

	protected static void chooseDifferentName() {
		new ShowErrorMessage("Choose different name!", "Please choose"
				+ " a different diskname since the one chosen is already"
				+ " used.");
	}


	private void deleteDiskAndTab(int deleteIndex) {
		if(listAss.size()==1) {
			listAss = new LinkedList<MenuAssignment>();
			wallList = new LinkedList<Line>();
			createButtonsAndLabel();
			splitpane.remove(2);
			splitpane.setRightComponent(getRightPanel());
			splitpane.setLeftComponent(new LeftPaintPanel(this, listAss, wallList));
			restoreDefaultSplitpaneDividerLocation(splitpane);
		} else if (listAss.size()>0) {
			String diskToDelete = listAss.get(deleteIndex).thisDisk.diskName;
			tabpane.removeTabAt(deleteIndex);
			listAss.remove(listAss.get(deleteIndex));
			chooserPointer.deleteBehaviourWithReferenceDisk(diskToDelete);				

			updateTabpane(tabpane.getSelectedIndex());
		}
	}


	private void changeDisk(final int index) {
		final AssignMenuDisk d = new AssignMenuDisk(listAss.get(index).thisDisk);

		d.buttonOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed (ActionEvent e)
			{
				if(e.getSource() == d.buttonOK) {
					d.diskName = d.tf1.getText();
					try
					{
						d.x = Double.parseDouble(d.tf2.getText())/100.0;
						d.y = Double.parseDouble(d.tf3.getText())/100.0;
						d.radius = Double.parseDouble(d.tf4.getText())/100.0;
						d.setVisible(false);
					} catch(NumberFormatException nfe) {
					}

					listAss.get(index).thisDisk.diskName = d.diskName;
					listAss.get(index).thisDisk.x = d.x;
					listAss.get(index).thisDisk.y = d.y;
					listAss.get(index).thisDisk.radius = d.radius;
					listAss.get(index).thisDisk.col = d.col;

					updateTabpane(index);
				}
			}
		});
	}

	/**Returns complete "tabPanel" for a MenuDisk d.
	 * 
	 * @param d
	 * @param interactionsChosen
	 * @return
	 */
	private JPanel getPanelForDisk(MenuAssignment currAss) {
		JPanel bigPanel = new JPanel();
		bigPanel.setLayout(new BorderLayout());
		bigPanel.setBackground(Color.white);

		JPanel diskDataPanel = getPanelForDiskHelper(currAss.thisDisk);

		bigPanel.add(diskDataPanel, BorderLayout.NORTH);		

		ChooseEvents eventsChoser = new ChooseEvents(listAss, currAss, leftPaintPanel,
				splitpane, this);
		chooserPointer = eventsChoser;

		bigPanel.add(eventsChoser, BorderLayout.CENTER);

		return bigPanel;
	}

	private JPanel getPanelForDiskHelper(MenuDisk d) {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBackground(d.col);

		JPanel leftNestedPanel = new JPanel();
		leftNestedPanel.setLayout(new GridLayout(4,1));
		leftNestedPanel.setBackground(Color.white);

		JLabel label1 = new JLabel("Diskname: "+d.diskName);
		JLabel label2 = new JLabel("x pos:    "+d.x);
		JLabel label3 = new JLabel("y pos:    "+d.y);
		JLabel label4 = new JLabel("radius:   "+d.radius);

		leftNestedPanel.add(label1);
		leftNestedPanel.add(label2);
		leftNestedPanel.add(label3);
		leftNestedPanel.add(label4);

		panel.add(leftNestedPanel, BorderLayout.WEST);

		return panel;
	}

	protected static void restoreDefaultSplitpaneDividerLocation(final JSplitPane splitpane) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				splitpane.setDividerLocation(splitpane.getSize().width /2);
			}
		});
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new A_MenuMain();
			}
		});
	}

	/***************************************************************
	 ********** METHODS FOR ENVIRONMENT (PANEL)  *******************
	 **************************************************************/

	/**
	 * Runs the given demo in the environment panel
	 * @param demo
	 */
	protected void runDemo(Demo demo) {

		final double DT_PER_STEP = 0.01;
		final long miliSecondsPerStep = 5;

		Environment env = demo.getEnvironment();

		leftPanelEnvironment.setEnvironment(env);
		leftPanelEnvironment.getSettings().setFullView(env);
		leftPanelEnvironment.setPreferredSize(new Dimension(getEnvironmentPanelSize(), getEnvironmentPanelSize()));
		validate();

		running = new AtomicBoolean(true);
		//		addKeyListener(getKeyListener(running));
		AgentController[] controllers = demo.getControllers();
		AgentMapping[] mappings = demo.getAgentMappings();
		if (controllers == null)
			controllers = new AgentController[0];
		if (mappings == null)
			mappings = new AgentMapping[0];
		if (controllers.length != mappings.length)
			throw new IllegalArgumentException("length of arrays returned by getControllers() and getAgentMappings() do not agree!");
		showNames(demo.getTitle(), mappings);
		boolean settingsAdapted = false;

		for (int i = 0; i < controllers.length; i++) {
			if(((AgentControllerWithAssignment)controllers[i]).hasAssignment()) {
				Assignment a = ((AgentMappingWithAssignment)mappings[i]).getAssignment();
				a.listInteractions[a.getNumCurrentInteraction()].reset();
			}
		}
		
		while (running.get()) {
			if (!settingsAdapted) {
				settingsAdapted = demo.adaptVisualisationSettings(leftPanelEnvironment.getSettings());
			}
			long ts = System.currentTimeMillis();
			env.doTimeStep(DT_PER_STEP, mappings);
			for (int i = 0; i < controllers.length; i++) {
				if(((AgentControllerWithAssignment)controllers[i]).hasAssignment()) {
					((AgentControllerWithAssignment)controllers[i]).doTimeStep(((AgentMappingWithAssignment)mappings[i]).getAssignment(), mappings[i].getActuatorValues());
				} else {
					controllers[i].doTimeStep(mappings[i].getSensorValues(), mappings[i].getActuatorValues());
				}
			}
			long time = System.currentTimeMillis() - ts;
			long sleep = miliSecondsPerStep - time;
			if (sleep > 0) {
				try {
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
				}
			}
		}
	}

	protected void killSimulationThread() {
		running.set(false);
	}

	private static void showNames(String title, AgentMapping[] mappings) {
		System.out.println(title);
		System.out.println();
		for (int i = 0; i < mappings.length; i++) {
			System.out.print("Agent " + i + " sensors:");
			for (String s : mappings[i].getSensorNames()) {
				System.out.print(" " + s);
			}
			System.out.println();
			System.out.print("Agent " + i + " actuators:");
			for (String s : mappings[i].getActuatorNames()) {
				System.out.print(" " + s);
			}
			System.out.println();
		}
		System.out.println();
	}

	public static LinkedList<Wall> getBoundaryWalls() {
		LinkedList<Wall> walls = new LinkedList<Wall>();
		// these 4 walls are the outside walls of the frame
		walls.add(new Wall(new Line(new diskworld.linalg2D.Point(0, 0), new diskworld.linalg2D.Point(1, 0)), 0.01)); 
		walls.add(new Wall(new Line(new diskworld.linalg2D.Point(1, 0), new diskworld.linalg2D.Point(1, 1)), 0.01));
		walls.add(new Wall(new Line(new diskworld.linalg2D.Point(1, 1), new diskworld.linalg2D.Point(0, 1)), 0.01));
		walls.add(new Wall(new Line(new diskworld.linalg2D.Point(0, 1), new diskworld.linalg2D.Point(0, 0)), 0.01));
		return walls;
	}

	private void setDefaultEnvironment() {
		int panelSize = getEnvironmentPanelSize();
		env = new Environment(SIZE_OF_ENVIRONMENT, SIZE_OF_ENVIRONMENT,
				getBoundaryWalls());
		env.getFloor().fill(FloorCellType.EMPTY);
		leftPanelEnvironment.setEnvironment(env);
		leftPanelEnvironment.getSettings().setFullView(env);
		leftPanelEnvironment.setPreferredSize(new Dimension(panelSize, panelSize));
		leftPanelEnvironment.setMinimumSize(new Dimension(panelSize/2, panelSize/2));
		leftPanelEnvironment.setMaximumSize(new Dimension(panelSize, panelSize));
		//		killSimulationThread();
		env.doTimeStep(0.01);
		leftPanelEnvironment.revalidate();
	}

	protected void printNewSimulationStarted() {
		System.out.println();
		System.err.println("New Simulation started.");
	}




	/***************************************************************
	 ********** HELPER METHODS FOR FILE HANDLING *******************
	 **************************************************************/


	private void openFileChooser() {

		FileChooser chooser = new FileChooser();

		try{
			while(!chooser.isChosen) {
				Thread.sleep(1);
			}

			if(chooser.isCorrect) {

				createNewStory();
				Story myStory = chooser.myChosenStory;

				for(MenuAssignment ass : myStory.getListOfMenuAsssignments()) {
					listAss.add(ass);
				}
				roundAllValues(listAss, 2);

				for(Line line : myStory.getListOfWalls()) {
					wallList.add(line);
				}


				for(int i=0; i<listAss.size(); i++) {
					getNewTabAndUpdate(i);
				}
				setStoryName(myStory.getStoryname());
				setDefaultEnvironment();
			}

		} catch(InterruptedException e) {
		}
	}
}
