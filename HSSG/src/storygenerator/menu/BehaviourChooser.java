package storygenerator.menu;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DecimalFormat;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;

import storygenerator.Behaviour;
import storygenerator.Events;
import storygenerator.Interactions;
import diskworld.linalg2D.Point;

/**
 * important class for the menu. Provides 
 * 	-	a JPanel for every {@Link Interactions} and {@Link Events}; 
 *  - 	a framework for assigning them.
 *  If the user wants to create a new {@Link Interactions} or {@Link Events}
 *  in the menu, this class is responsible.
 *  If the programmer wants to add a new {@Link Interactions} or {@Link Events},
 *  it is necessary to add a JPanel etc. as well in this class.
 *
 */
public class BehaviourChooser extends JFrame {

	protected boolean isChosen = false;
	protected JButton buttonOK = new JButton("OK");

	private Behaviour behaviour;
	private static LinkedList<String> behaviourNames;
	private Behaviour previousBehaviour;
	private boolean isEvent;
	private boolean continueTrajectory = true;

	private LeftPaintPanel paintPanel;
	private JComboBox selectionComboBox;
	private JPanel cards;
	private JSlider impulseSlider;
	private JSlider angularSpeedSlider;
	private JSlider chaseDistanceSlider;
	private JSlider visibilitySlider;
	private JSlider invisibilitySlider;
	private JSlider proximitySlider;

	private static final String SELECT = "Select";
	private static JFormattedTextField rotateRadiusTextField;
	private static JFormattedTextField rotateAroundRadiusTextField;
	private static JFormattedTextField secSinceStartTextField;
	private static JFormattedTextField periodOfTimeTextField;
	private static JFormattedTextField yTextField;
	private static JFormattedTextField xTextField;
	private static MenuDisk refDisk;
	
	//ref disk for HAS_STOPPED can be the own disk as well in contrast
	// to the usual ref disk, which is one of the other disks.
	private static MenuDisk refDiskForHasStopped;


	public BehaviourChooser(LinkedList<MenuAssignment> listAss, MenuAssignment currAss,
			Behaviour previouslySelectedBehaviour, String previouslySelectedComboBoxItem,
			boolean doChooseAnEvent, LeftPaintPanel paintPanel,
			JSplitPane splitpane,
			A_MenuMain mainMenu) {

		if(null!=previouslySelectedBehaviour) {
			this.previousBehaviour = previouslySelectedBehaviour;
		}

		this.isEvent = doChooseAnEvent;
		this.paintPanel = paintPanel;
		if(null!=previouslySelectedBehaviour && 
				this.previousBehaviour.getName().equals("TRAJECTORY")) {
			this.paintPanel.setCurrentDrawItemToTrajectory(((Interactions)this.previousBehaviour).getTrajectory());
		} else {
			this.paintPanel.setCurrentDrawItemToTrajectory();
		}
		this.setAlwaysOnTop(true);

		JPanel panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		cards = new JPanel(new CardLayout());

		if(!paintPanel.isShowing()) {
			splitpane.setLeftComponent(paintPanel);
			mainMenu.killSimulationThread();
			A_MenuMain.restoreDefaultSplitpaneDividerLocation(splitpane);
		}


		if(!isEvent) {
			behaviourNames = new LinkedList<String>();
			behaviourNames.add(SELECT);
			for(int i=1; i<Interactions.NUM_OVERALL_INTERACTIONS; i++) {
				if( ! (listAss.size()==1 && Interactions.referenceDiskNeeded(i))) {
					//prevent showing Interactions that require a reference disk if 
					//there is only 1 disk.
					behaviourNames.add(Interactions.getInteractionName(i));
				}
			}

			selectionComboBox = new JComboBox(getComboBoxItemsForBehaviours());
			selectionComboBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent evt) {
					String iaName = (String)evt.getItem();
					CardLayout cl = (CardLayout)(cards.getLayout());
					cl.show(cards, iaName);
				}
			});
			selectionComboBox.setEditable(false);

			setTitle("Assigning an Interaction");

			for(String s : behaviourNames) {
				JPanel p;
				switch(s) {
				case SELECT:
					p = getSelectCard();
					p.setName(s);
					cards.add(p, SELECT);
					break;
				case "PHYSICALLY":
					p = getPhysicallyDiskCard();
					p.setName(s);
					cards.add(p, s);
					break;
				case "RANDOMLY":
					p = getDefaultDiskCard();
					p.setName(s);
					cards.add(p, s);
					break;
				case "TRAJECTORY":
					p = getTrajectoryDiskCard();
					p.setName(s);
					cards.add(p, s);
					break;
				case "ROTATE":
					p = getROTATE_Card();
					p.setName(s);
					cards.add(p, s);
					break;
				case "STOP":
					p = getDefaultDiskCard();
					p.setName(s);
					cards.add(p, s);
					break;
				case "CHASE":
					String explanation1 = "chased";
					p = getChaseCard(listAss, currAss, explanation1);
					p.setName(s);
					cards.add(p, s);
					break;
				case "AVOID":
					String explanation2 = "avoided";
					p = getReferenceDiskCard(listAss, currAss, explanation2, false);
					p.setName(s);
					cards.add(p, s);
					break;
				case "TURN_T0":
					String explanation3 = "the one you turn to";
					p = getTurnToDiskCard(listAss, currAss, explanation3);
					p.setName(s);
					cards.add(p, s);
					break;
				case "AVOID_AND_ROTATE":
					String explanation4 = "avoided";
					p = getReferenceDiskCard(listAss, currAss, explanation4, false);
					p.setName(s);
					cards.add(p, s);
					break;
				case "ROTATE_AROUND":
					p = getROTATE_AROUND_Card(listAss, currAss);
					p.setName(s);
					cards.add(p, s);
					break;
				case "DISAPPEAR":
					p = getDefaultDiskCard();
					p.setName(s);
					cards.add(p, s);
					break;
				}
			}
		} else { //select an Event

			behaviourNames = new LinkedList<String>();
			behaviourNames.add(SELECT);
			for(int i=1; i<=Events.NUM_OVERALL_EVENTS; i++) {
				if( ! (listAss.size()==1 && Events.referenceDiskNeeded(i))) {
					//prevent showing Events that require a reference disk if 
					//there is only 1 disk.
					behaviourNames.add(Events.getEventName(i));
				}
			}

			selectionComboBox = new JComboBox(getComboBoxItemsForBehaviours());
			selectionComboBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent evt) {
					String eventName = (String)evt.getItem();
					CardLayout cl = (CardLayout)(cards.getLayout());
					cl.show(cards, eventName);
				}
			});
			selectionComboBox.setEditable(false);

			setTitle("Assigning an Event");

			for(String s : behaviourNames) {
				JPanel p;
				switch(s) {
				case SELECT:
					p = getSelectCard();
					p.setName(s);
					cards.add(p, SELECT);
					break;
				case "ALWAYS":
					p = getDefaultDiskCard();
					p.setName(s);
					cards.add(p, s);
					break;
				case "SEC_SINCE_START":
					p = getSEC_SINCE_START_Card(listAss, currAss);
					p.setName(s);
					cards.add(p, s);
					break;
				case "PERIOD_OF_TIME":
					p = getPERIOD_OF_TIME_Card(listAss, currAss);
					p.setName(s);
					cards.add(p, s);
					break;
				case "TOUCH_OBJECT":
					String explanation1 = "being touched";
					p = getReferenceDiskCard(listAss, currAss, explanation1, false);
					p.setName(s);
					cards.add(p, s);
					break;
				case "SAME_X_POS":
					String explanation2 = "at the same x position";
					p = getReferenceDiskCard(listAss, currAss, explanation2, false);
					p.setName(s);
					cards.add(p, s);
					break;
				case "SAME_Y_POS":
					String explanation3 = "at the same y position";
					p = getReferenceDiskCard(listAss, currAss, explanation3, false);
					p.setName(s);
					cards.add(p, s);
					break;
				case "TURN_IS_OVER":
					String explanation4 = "the one you want to turn to";
					p = getReferenceDiskCard(listAss, currAss, explanation4, false);
					p.setName(s);
					cards.add(p, s);
					break;
				case "BECAME_VISIBLE":
					String explanation5 = "visible / invisible";
					p = getVisibleCard(listAss, currAss, explanation5);
					p.setName(s);
					cards.add(p, s);
					break;
				case "BECAME_INVISIBLE":
					String explanation6 = "visible / invisible";
					p = getInvisibleCard(listAss, currAss, explanation6);
					p.setName(s);
					cards.add(p, s);
					break;
				case "CLOSE":
					String explanation7 = "close";
					p = getCloseCard(listAss, currAss, explanation7);
					p.setName(s);
					cards.add(p, s);
					break;
				case "HAS_STOPPED":
					String explanation8 = "stopping";
					p = getHAS_STOPPED_Card(listAss, currAss, explanation8);
					p.setName(s);
					cards.add(p, s);
					break;
				}
			}
		}

		setCardsToPreviousSelection(previouslySelectedComboBoxItem);

		setSize(600, 200);
		JPanel comboBoxPane = new JPanel();
		getContentPane().add(comboBoxPane, BorderLayout.PAGE_START);
		getContentPane().add(cards, BorderLayout.CENTER);
		comboBoxPane.add(selectionComboBox);
		panel1.add(cards, BorderLayout.CENTER);
		panel1.add(buttonOK, BorderLayout.SOUTH);
		getRootPane().setDefaultButton(buttonOK); 
		add(panel1);


		//setLocation
		int height = Toolkit.getDefaultToolkit().getScreenSize().height;			
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice defaultScreen = ge.getDefaultScreenDevice();
		Rectangle rect = defaultScreen.getDefaultConfiguration().getBounds();
		int x = (int) rect.getMaxX() - getWidth();
		int y = height / 2;
		this.setLocation(x, y);

		setResizable(false);
		setVisible(true);
	}

	/**
	 * set selected Card to the card that matches the previous selection
	 */
	private void setCardsToPreviousSelection(String previouslySelectedComboBoxItem) {
		CardLayout cardLayout = (CardLayout) cards.getLayout();


		int index = 0;
		boolean previousSelectionCardExists = false;
		for(String s : behaviourNames) {
			if(s.equals(previouslySelectedComboBoxItem)) {
				previousSelectionCardExists = true;
				break;
			}
			index++;
		}

		if(previousSelectionCardExists) {
			cardLayout.show(cards, previouslySelectedComboBoxItem);
			selectionComboBox.setSelectedIndex(index);
		} else {
			cardLayout.show(cards, previouslySelectedComboBoxItem);
			selectionComboBox.setSelectedIndex(0);
		}
	}



	private JPanel getDefaultDiskCard() {
		JPanel card = new JPanel();
		JLabel label = new JLabel("Please select OK");
		card.add(label);
		return card;
	}
	
	private JPanel getPhysicallyDiskCard() {
		JPanel card = new JPanel();
		
		card.setLayout(new GridLayout(2,1));
		
		JPanel p = new JPanel();
		JLabel l1 = new JLabel("                             ");
		JLabel l2 = new JLabel("                             ");
		JLabel l3 = new JLabel("Please select the size of the impulse that the disk will get.", SwingConstants.CENTER);
		p.setLayout(new BorderLayout());
		
		final int MIN = 0;
		final int MAX = 100;
		int initValue;
		
		if(null != previousBehaviour && ((Interactions)previousBehaviour).impulseNeeded()) {
			initValue = (int)(100.0*((Interactions)previousBehaviour).getImpulseSize());
		} else {
			initValue = 50;
		}
		
		impulseSlider = new JSlider(JSlider.HORIZONTAL, MIN, MAX, initValue);
		impulseSlider.setMajorTickSpacing(10);
		impulseSlider.setMinorTickSpacing(5);
		impulseSlider.setPaintTicks(true);
		impulseSlider.setPaintLabels(true);
		p.add(impulseSlider, BorderLayout.CENTER);
		p.add(l1, BorderLayout.EAST);
		p.add(l2, BorderLayout.WEST);
		p.add(l3, BorderLayout.NORTH);
	
		card.add(p);
		
		return card;
	}
	
	private JPanel getChaseCard(LinkedList<MenuAssignment> listAss, MenuAssignment currAss, String explanation) {
		JPanel card = new JPanel();
		
		card.setLayout(new GridLayout(2,1));
		
		JPanel p = new JPanel();
		JLabel l1 = new JLabel("                             ");
		JLabel l2 = new JLabel("                             ");
		JLabel l3 = new JLabel("Please select the distance of the chase. 0: no distance,"
				+ " 100: half of the screen size.", SwingConstants.CENTER);
		p.setLayout(new BorderLayout());
		
		final int MIN = 0;
		final int MAX = 100;
		int initValue;
		
		if(null != previousBehaviour && ((Interactions)previousBehaviour).chasingDistanceNeeded()) {
			initValue = (int)(100.0*((Interactions)previousBehaviour).getChasingDistance());
		} else {
			initValue = 50;
		}
		
		chaseDistanceSlider = new JSlider(JSlider.HORIZONTAL, MIN, MAX, initValue);
		chaseDistanceSlider.setMajorTickSpacing(10);
		chaseDistanceSlider.setMinorTickSpacing(5);
		chaseDistanceSlider.setPaintTicks(true);
		chaseDistanceSlider.setPaintLabels(true);
		p.add(chaseDistanceSlider, BorderLayout.CENTER);
		p.add(l1, BorderLayout.EAST);
		p.add(l2, BorderLayout.WEST);
		p.add(l3, BorderLayout.NORTH);
	
		card.add(p);
		
		JPanel diskSelectionPanel = getReferenceDiskChoserPanel(listAss, currAss, explanation, false);
		card.add(diskSelectionPanel);
		return card;
	}
	
	private JPanel getInvisibleCard(LinkedList<MenuAssignment> listAss, MenuAssignment currAss, String explanation) {
		JPanel card = new JPanel();
		
		card.setLayout(new GridLayout(2,1));
		
		JPanel p = new JPanel();
		JLabel l1 = new JLabel("                             ");
		JLabel l2 = new JLabel("                             ");
		JLabel l3 = new JLabel("Please select the angle of the field of vision (in degrees). 30: small,"
				+ " 360: full", SwingConstants.CENTER);
		p.setLayout(new BorderLayout());
		
		final int MIN = 30;
		final int MAX = 360;
		int initValue;
		
		if(null != previousBehaviour && ((Events)previousBehaviour).angleOfVisibilityNeeded()) {
			initValue = ((Events)previousBehaviour).getAngleOfVisibility();
		} else {
			initValue = 90;
		}
		
		invisibilitySlider = new JSlider(JSlider.HORIZONTAL, MIN, MAX, initValue);
		invisibilitySlider.setMajorTickSpacing(30);
		invisibilitySlider.setMinorTickSpacing(15);
		invisibilitySlider.setPaintTicks(true);
		invisibilitySlider.setPaintLabels(true);
		p.add(invisibilitySlider, BorderLayout.CENTER);
		p.add(l1, BorderLayout.EAST);
		p.add(l2, BorderLayout.WEST);
		p.add(l3, BorderLayout.NORTH);
	
		card.add(p);
		
		JPanel diskSelectionPanel = getReferenceDiskChoserPanel(listAss, currAss, explanation, false);
		card.add(diskSelectionPanel);
		return card;
	}
	
	private JPanel getCloseCard(LinkedList<MenuAssignment> listAss, MenuAssignment currAss, String explanation) {
		JPanel card = new JPanel();
		
		card.setLayout(new GridLayout(2,1));
		
		JPanel p = new JPanel();
		JLabel l1 = new JLabel("                             ");
		JLabel l2 = new JLabel("                             ");
		JLabel l3 = new JLabel("Please select the proximity. 0: no distance,"
				+ " 100: screen size", SwingConstants.CENTER);
		p.setLayout(new BorderLayout());
		
		final int MIN = 0;
		final int MAX = 100;
		int initValue;
		
		if(null != previousBehaviour && ((Events)previousBehaviour).relativeProximityNeeded()) {
			initValue = (int)(100.0*(((Events)previousBehaviour).getRelativeProximity()));
		} else {
			initValue = 25;
		}
		
		proximitySlider = new JSlider(JSlider.HORIZONTAL, MIN, MAX, initValue);
		proximitySlider.setMajorTickSpacing(10);
		proximitySlider.setMinorTickSpacing(5);
		proximitySlider.setPaintTicks(true);
		proximitySlider.setPaintLabels(true);
		p.add(proximitySlider, BorderLayout.CENTER);
		p.add(l1, BorderLayout.EAST);
		p.add(l2, BorderLayout.WEST);
		p.add(l3, BorderLayout.NORTH);
	
		card.add(p);
		
		JPanel diskSelectionPanel = getReferenceDiskChoserPanel(listAss, currAss, explanation, false);
		card.add(diskSelectionPanel);
		return card;
	}
	
	private JPanel getVisibleCard(LinkedList<MenuAssignment> listAss, MenuAssignment currAss, String explanation) {
		JPanel card = new JPanel();
		
		card.setLayout(new GridLayout(2,1));
		
		JPanel p = new JPanel();
		JLabel l1 = new JLabel("                             ");
		JLabel l2 = new JLabel("                             ");
		JLabel l3 = new JLabel("Please select the angle of the field of vision (in degrees). 30: small,"
				+ " 360: full", SwingConstants.CENTER);
		p.setLayout(new BorderLayout());
		
		final int MIN = 30;
		final int MAX = 360;
		int initValue;
		
		if(null != previousBehaviour && ((Events)previousBehaviour).angleOfVisibilityNeeded()) {
			initValue = ((Events)previousBehaviour).getAngleOfVisibility();
		} else {
			initValue = 90;
		}
		
		visibilitySlider = new JSlider(JSlider.HORIZONTAL, MIN, MAX, initValue);
		visibilitySlider.setMajorTickSpacing(30);
		visibilitySlider.setMinorTickSpacing(15);
		visibilitySlider.setPaintTicks(true);
		visibilitySlider.setPaintLabels(true);
		p.add(visibilitySlider, BorderLayout.CENTER);
		p.add(l1, BorderLayout.EAST);
		p.add(l2, BorderLayout.WEST);
		p.add(l3, BorderLayout.NORTH);
	
		card.add(p);
		
		JPanel diskSelectionPanel = getReferenceDiskChoserPanel(listAss, currAss, explanation, false);
		card.add(diskSelectionPanel);
		return card;
	}
	
	private JPanel getTurnToDiskCard(LinkedList<MenuAssignment> listAss, MenuAssignment currAss, String explanation) {
		JPanel card = new JPanel();
		
		card.setLayout(new GridLayout(2,1));
		
		JPanel p = new JPanel();
		JLabel l1 = new JLabel("                             ");
		JLabel l2 = new JLabel("                             ");
		JLabel l3 = new JLabel("Please select the speed of the rotation.", SwingConstants.CENTER);
		p.setLayout(new BorderLayout());
		
		final int MIN = 10;
		final int MAX = 100;
		int initValue;
		
		if(null != previousBehaviour && ((Interactions)previousBehaviour).angularSpeedNeeded()) {
			initValue = (int)(10000.0*((Interactions)previousBehaviour).getAngularSpeed());
		} else {
			initValue = 50;
		}
		
		angularSpeedSlider = new JSlider(JSlider.HORIZONTAL, MIN, MAX, initValue);
		angularSpeedSlider.setMajorTickSpacing(10);
		angularSpeedSlider.setMinorTickSpacing(5);
		angularSpeedSlider.setPaintTicks(true);
		angularSpeedSlider.setPaintLabels(true);
		p.add(angularSpeedSlider, BorderLayout.CENTER);
		p.add(l1, BorderLayout.EAST);
		p.add(l2, BorderLayout.WEST);
		p.add(l3, BorderLayout.NORTH);
	
		card.add(p);
		
		JPanel diskSelectionPanel = getReferenceDiskChoserPanel(listAss, currAss, explanation, false);
		card.add(diskSelectionPanel);
		return card;
	}
	
	private JPanel getSorryDiskCard() {
		JPanel card = new JPanel();
		JLabel label = new JLabel("Sorry - not yet implemented");
		card.add(label);
		return card;
	}

	private JPanel getTrajectoryDiskCard() {
		JPanel card = new JPanel();
		card.setLayout(new GridLayout(2,1));
		String text = "Please create a trajectory by clicking on "
				+ "the black area on the left.";
		JLabel label = new JLabel(text, SwingConstants.CENTER);
		card.add(label);
		
		boolean continueTrajectory = true;
		if(null != previousBehaviour) {
			continueTrajectory = ((Interactions)previousBehaviour).doContinueTrajectory();
		}
		
		JPanel questionPanel = getYesNoPanelForTrajectory("Should the disk continue "
				+ "its trajectory from startpoint after having"
				+ " completed the trajectory?", continueTrajectory);
		card.add(questionPanel);
		return card;
	}

	private JPanel getReferenceDiskCard(LinkedList<MenuAssignment> listAss,
			MenuAssignment currAss, String explanation, boolean includeThisDisk) {
		JPanel card = new JPanel();
		JPanel diskSelectionPanel = getReferenceDiskChoserPanel(listAss, currAss, explanation, includeThisDisk);
		card.add(diskSelectionPanel);

		return card;
	}
	
	private JPanel getHAS_STOPPED_Card(LinkedList<MenuAssignment> listAss,
			MenuAssignment currAss, String explanation) {
		JPanel card = new JPanel();
		JPanel diskSelectionPanel = getReferenceDiskChoserPanelForHasStopped(listAss, currAss, explanation, true);
		card.add(diskSelectionPanel);

		return card;
	}


	private JPanel getSelectCard() {
		JPanel card = new JPanel();
		JLabel label = new JLabel("Please select.");
		card.add(label);
		return card;
	}

	private JPanel getROTATE_AROUND_Card(LinkedList<MenuAssignment> listAss,
			MenuAssignment currAss) {
		JPanel card = new JPanel();
		card.setLayout(new GridLayout(2,1));

		JPanel radiusPanel = getRadiusPanelForROTATE_AROUND();
		card.add(radiusPanel);

		String explanation = "the center of rotation";

		JPanel diskSelectionPanel = getReferenceDiskChoserPanel(listAss, currAss, explanation, false);
		card.add(diskSelectionPanel);
		return card;
	}

	private JPanel getROTATE_Card() {
		JPanel card = new JPanel();
		card.setLayout(new GridLayout(2,1));

		JPanel radiusPanel = getRadiusPanelForROTATE();
		JPanel pointPanel = getPointPanel();
		card.add(radiusPanel);
		card.add(pointPanel);

		return card;
	}

	private JPanel getSEC_SINCE_START_Card(LinkedList<MenuAssignment> listAss,
			MenuAssignment currAss) {
		JPanel card = new JPanel();
		card.setLayout(new GridLayout(2,1));
		JPanel timePanel = getTimePanelForSEC_SINCE_START();
		card.add(timePanel);
		return card;
	}

	private JPanel getTimePanelForSEC_SINCE_START() {
		JPanel timePanel = new JPanel();
		JLabel label = new JLabel("Please specify the desired time since "
				+ "start (in seconds): ");
		secSinceStartTextField = new JFormattedTextField(new DecimalFormat("00"));
		secSinceStartTextField.setColumns(4);
		secSinceStartTextField.setForeground(Color.BLUE);
		secSinceStartTextField.setBackground(Color.YELLOW);
		secSinceStartTextField.setText("5");

		if(isEvent && previousBehaviour != null) {
			if(previousBehaviour.getName().equals("SEC_SINCE_START")) {
				secSinceStartTextField.setText(Integer.toString((int)(((Events)previousBehaviour).getTargetTimeOfEvent())));
			}
		}

		timePanel.add(label);
		timePanel.add(secSinceStartTextField);

		return timePanel;
	}

	private JPanel getPERIOD_OF_TIME_Card(LinkedList<MenuAssignment> listAss,
			MenuAssignment currAss) {
		JPanel card = new JPanel();
		card.setLayout(new GridLayout(2,1));
		JPanel timePanel = getTimePanelForPERIOD_OF_TIME();
		card.add(timePanel);
		return card;
	}

	private JPanel getTimePanelForPERIOD_OF_TIME() {
		JPanel timePanel = new JPanel();
		JLabel label = new JLabel("Please specify the desired time period"
				+ " (in seconds): ");
		periodOfTimeTextField = new JFormattedTextField(new DecimalFormat("00"));
		periodOfTimeTextField.setColumns(4);
		periodOfTimeTextField.setForeground(Color.BLUE);
		periodOfTimeTextField.setBackground(Color.YELLOW);
		periodOfTimeTextField.setText("5");

		if(isEvent && previousBehaviour != null) {
			if(previousBehaviour.getName().equals("PERIOD_OF_TIME")) {
				periodOfTimeTextField.setText(Double.toString(((Events)previousBehaviour).getTargetTimeOfEvent()));
			}
		}

		timePanel.add(label);
		timePanel.add(periodOfTimeTextField);

		return timePanel;
	}

	private JPanel getPointPanel() {
		JPanel pointPanel = new JPanel();
		JLabel label = new JLabel("Please specify the x and y coordinates of the Center of Rotation: ");
		xTextField = new JFormattedTextField(new DecimalFormat("00"));
		xTextField.setColumns(4);
		xTextField.setForeground(Color.BLUE);
		xTextField.setBackground(Color.YELLOW);
		xTextField.setText("50");

		yTextField = new JFormattedTextField(new DecimalFormat("00"));
		yTextField.setColumns(4);
		yTextField.setForeground(Color.BLUE);
		yTextField.setBackground(Color.YELLOW);
		yTextField.setText("50");

		if(!isEvent && previousBehaviour != null) {
			if(previousBehaviour.getName().equals("ROTATE")) {				
				xTextField.setText(Double.toString(100.0*((Interactions)previousBehaviour).getCenterOfRotationPoint().x));
				yTextField.setText(Double.toString(100.0*((Interactions)previousBehaviour).getCenterOfRotationPoint().y));
			}
		}

		pointPanel.add(label);
		pointPanel.add(xTextField);
		pointPanel.add(yTextField);

		return pointPanel;
	}

	private JPanel getRadiusPanelForROTATE() {
		JPanel radiusPanel = new JPanel();
		JLabel label = new JLabel("Please specify the radius of the rotation: ");
		rotateRadiusTextField = new JFormattedTextField(new DecimalFormat("00"));
		rotateRadiusTextField.setColumns(4);
		rotateRadiusTextField.setForeground(Color.BLUE);
		rotateRadiusTextField.setBackground(Color.YELLOW);
		rotateRadiusTextField.setText("15");

		if(!isEvent && previousBehaviour != null) {
			if(previousBehaviour.getName().equals("ROTATE")) {
				rotateRadiusTextField.setText(Double.toString(100.0*((Interactions)previousBehaviour).getRadiusOfRotation()));
			}
		}

		radiusPanel.add(label);
		radiusPanel.add(rotateRadiusTextField);

		return radiusPanel;
	}

	private JPanel getRadiusPanelForROTATE_AROUND() {
		JPanel radiusPanel = new JPanel();
		JLabel label = new JLabel("Please specify the radius of the rotation: ");
		rotateAroundRadiusTextField = new JFormattedTextField(new DecimalFormat("00"));
		rotateAroundRadiusTextField.setColumns(4);
		rotateAroundRadiusTextField.setForeground(Color.BLUE);
		rotateAroundRadiusTextField.setBackground(Color.YELLOW);
		rotateAroundRadiusTextField.setText("15");

		if(!isEvent && null != previousBehaviour) {
			if(previousBehaviour.getName().equals("ROTATE_AROUND")) {
				rotateAroundRadiusTextField.setText(Double.toString(100.0*((Interactions)previousBehaviour).getRadiusOfRotation()));
			}
		}

		radiusPanel.add(label);
		radiusPanel.add(rotateAroundRadiusTextField);

		return radiusPanel;
	}

	private JPanel getYesNoPanelForTrajectory(String question, boolean defaultOption) {

		JPanel panel = new JPanel();

		JLabel label = new JLabel(question);
		
		String comboBoxItems[] = new String[2];
		
		if(defaultOption) {
			comboBoxItems[0] = "YES";
			comboBoxItems[1] = "NO";
		} else {
			comboBoxItems[0] = "NO";
			comboBoxItems[1] = "YES";
		}
		JComboBox cb = new JComboBox(comboBoxItems);

		cb.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent evt) {

				String nameOfSelected = (String)evt.getItem();
				if(nameOfSelected.equals("NO")) {
					continueTrajectory = false;
				} else {
					continueTrajectory = true;
				}
			}
		});

		cb.setEditable(false);			
		panel.add(label);
		panel.add(cb);
		return panel;
	}
	
	private JPanel getReferenceDiskChoserPanelForHasStopped(final LinkedList<MenuAssignment> listAss,
			MenuAssignment currAss, String explanation, boolean includeThisDisk) {

		JPanel panel = new JPanel();

		JLabel label = new JLabel("Please select a reference disk (the disk that is "+explanation+")");

		LinkedList<MenuDisk> referenceDiskList = new LinkedList<MenuDisk>();

		boolean defaultRefDiskAssigned = false;
		for(MenuAssignment assignment : listAss) {
			if(includeThisDisk || ! assignment.thisDisk.diskName.equals(currAss.thisDisk.diskName)) {
				referenceDiskList.add(assignment.thisDisk);
				if(!defaultRefDiskAssigned) {
					// set the disk that is shown at the top as default reference disk.
					// this default Disk is chosen as referenceDisk if nothing is 
					// selected from the ComboBox.
					refDiskForHasStopped = assignment.thisDisk;
					defaultRefDiskAssigned = true;
				}
			}
		}

		String comboBoxItems[] = new String[referenceDiskList.size()];

		for(int j=0; j<comboBoxItems.length; j++) {
			comboBoxItems[j] = referenceDiskList.get(j).diskName;
		}
		JComboBox cb = new JComboBox(comboBoxItems);

		cb.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent evt) {
				String nameOfSelected = (String)evt.getItem();

				for(MenuAssignment assignment : listAss) {
					if(nameOfSelected.equals(assignment.thisDisk.diskName)) {
						refDiskForHasStopped = assignment.thisDisk;
					}
				}
			}
		});

		if(previousBehaviour!=null) {
			if(previousBehaviour.referenceDiskNeeded()) {
				int counter = 0;
				for(MenuDisk d : referenceDiskList) {
					if(previousBehaviour.getMenuReferenceDiskName().equals(d.diskName)) {
						cb.setSelectedIndex(counter);
						refDiskForHasStopped = d;
					}
					counter++;
				}
			}
		}

		cb.setEditable(false);			
		panel.add(label);
		panel.add(cb);
		return panel;
	}

	/**
	 * 
	 * @param listAss
	 * @param currAss
	 * @param explanation - short explanation what reference disk means in this case
	 * @return
	 */
	private JPanel getReferenceDiskChoserPanel(final LinkedList<MenuAssignment> listAss,
			MenuAssignment currAss, String explanation, boolean includeThisDisk) {

		JPanel panel = new JPanel();

		JLabel label = new JLabel("Please select a reference disk (the disk that is "+explanation+")");

		LinkedList<MenuDisk> referenceDiskList = new LinkedList<MenuDisk>();

		boolean defaultRefDiskAssigned = false;
		for(MenuAssignment assignment : listAss) {
			if(includeThisDisk || ! assignment.thisDisk.diskName.equals(currAss.thisDisk.diskName)) {
				referenceDiskList.add(assignment.thisDisk);
				if(!defaultRefDiskAssigned) {
					// set the disk that is shown at the top as default reference disk.
					// this default Disk is chosen as referenceDisk if nothing is 
					// selected from the ComboBox.
					refDisk = assignment.thisDisk;
					defaultRefDiskAssigned = true;
				}
			}
		}

		String comboBoxItems[] = new String[referenceDiskList.size()];

		for(int j=0; j<comboBoxItems.length; j++) {
			comboBoxItems[j] = referenceDiskList.get(j).diskName;
		}
		JComboBox cb = new JComboBox(comboBoxItems);

		cb.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent evt) {
				String nameOfSelected = (String)evt.getItem();

				for(MenuAssignment assignment : listAss) {
					if(nameOfSelected.equals(assignment.thisDisk.diskName)) {
						refDisk = assignment.thisDisk;
					}
				}
			}
		});

		if(previousBehaviour!=null) {
			if(previousBehaviour.referenceDiskNeeded()) {
				int counter = 0;
				for(MenuDisk d : referenceDiskList) {
					if(previousBehaviour.getMenuReferenceDiskName().equals(d.diskName)) {
						cb.setSelectedIndex(counter);
						refDisk = d;
					}
					counter++;
				}
			}
		}

		cb.setEditable(false);			
		panel.add(label);
		panel.add(cb);
		return panel;
	}


	private  String[] getComboBoxItemsForBehaviours() {
		String comboBoxItems[] = new String[behaviourNames.size()];
		for(int i=0; i<comboBoxItems.length; i++) {
			comboBoxItems[i] = behaviourNames.get(i);
		}
		return comboBoxItems;
	}

	/**
	 * called by ChooseEvents every time the buttonOK is pressed.
	 * @return
	 */
	private void updateSelectedInteraction() {
		JPanel currentCard = null;
		for (Component comp : cards.getComponents() ) {
			if (comp.isVisible() == true) {
				currentCard = (JPanel)comp;
			}
		}
		String iaName = currentCard.getName();

		if(iaName.equals(SELECT)) {
			new ShowErrorMessage("Select first", "Please select an "
					+ "Interaction before selecting 'OK'.");
		} else if((iaName.equals("TRAJECTORY") || iaName.equals("TRAJ_CONTINUOUSLY"))
				&& (!paintPanel.isTrajectoryChosen())) {

			new ShowErrorMessage("Create trajectory first", 
					"Please create a trajectory by clicking on the black area"
							+ " on the left.");
		} else {
			boolean inputIsOk = true;
			switch(iaName) {
			case "TRAJECTORY":
				behaviour = new Interactions(Interactions.getInteractionsNum(iaName),
						paintPanel.getTrajectory(), continueTrajectory);
				break;

			case "PHYSICALLY":
				double impulseSize = impulseSlider.getValue() / 100.0;
				behaviour = new Interactions(Interactions.getInteractionsNum(iaName), impulseSize);
				break;
				
			case "RANDOMLY":
			case "DISAPPEAR":
			case "STOP":
				behaviour = new Interactions(Interactions.getInteractionsNum(iaName));
				break;

			case "ROTATE":
				double x = Double.parseDouble(xTextField.getText())/100.0;
				double y = Double.parseDouble(yTextField.getText())/100.0;
				double r1 = Double.parseDouble(rotateRadiusTextField.getText())/100.0;

				boolean wrongInput = A_MenuMain.isWrongInputAndShowError(x,y,r1);
				if(wrongInput) {
					inputIsOk = false;
				} else {
					Point centerOfRotation = new Point(x, y);
					behaviour = new Interactions(Interactions.getInteractionsNum(iaName), centerOfRotation, r1);
				}
				break;
				
			case "TURN_T0":
				double angularSpeedValue = angularSpeedSlider.getValue() / 10000.0;
				behaviour = new Interactions(Interactions.getInteractionsNum(iaName), refDisk.diskName, angularSpeedValue);
				break;

			case "CHASE":
				double chasingDistanceValue = chaseDistanceSlider.getValue() / 100.0;
				behaviour = new Interactions(Interactions.getInteractionsNum(iaName), refDisk.diskName, chasingDistanceValue);
				break;
				
			case "AVOID":
			case "AVOID_AND_ROTATE":

				behaviour = new Interactions(Interactions.getInteractionsNum(iaName), refDisk.diskName);
				break;						

			case "ROTATE_AROUND":
				double r2 = Double.parseDouble(rotateAroundRadiusTextField.getText())/100.0;

				boolean wrongInput2 = A_MenuMain.isWrongInputAndShowError(0.42,0.42,r2);
				if(wrongInput2) {
					inputIsOk = false;
				} else {

					behaviour = new Interactions(Interactions.getInteractionsNum(iaName), refDisk.diskName, r2);
				}
				break;
			}

			if(inputIsOk) {
				this.paintPanel.setCurrentDrawItemToDefault();
				isChosen = true;
				dispose();
			}
		}
	}

	/**
	 * called by ChooseEvents every time the buttonOK is pressed.
	 * @return
	 */
	private void updateSelectedEvent() {
		JPanel currentCard = null;
		for (Component comp : cards.getComponents() ) {
			if (comp.isVisible() == true) {
				currentCard = (JPanel)comp;
			}
		}
		String evName = currentCard.getName();

		if(evName.equals(SELECT)) {
			new ShowErrorMessage("Select first", "Please select an "
					+ "Interaction before selecting 'OK'.");
		} else {
			switch(evName) {

			case "ALWAYS":
				behaviour = new Events(Events.getEventsNum(evName));
				break;
				
			case "HAS_STOPPED":
				behaviour = new Events(Events.getEventsNum(evName), refDiskForHasStopped.diskName);
				break;

			case "TOUCH_OBJECT":
			case "SAME_X_POS":
			case "SAME_Y_POS":
			case "TURN_IS_OVER":
				behaviour = new Events(Events.getEventsNum(evName), refDisk.diskName);
				break;
				
			case "CLOSE":
				double relativeProximity = proximitySlider.getValue() / 100.0;
				behaviour = new Events(Events.getEventsNum(evName), refDisk.diskName, relativeProximity);
				break;

			case "SEC_SINCE_START":
				double p1 = Double.parseDouble(secSinceStartTextField.getText());
				behaviour = new Events(Events.getEventsNum(evName), p1);
				break;

			case "PERIOD_OF_TIME":
				double p2 = Double.parseDouble(periodOfTimeTextField.getText());
				behaviour = new Events(Events.getEventsNum(evName), p2);
				break;
				
			case "BECAME_VISIBLE":
				int angleOfVisibility = visibilitySlider.getValue();
				behaviour = new Events(Events.getEventsNum(evName), refDisk.diskName, angleOfVisibility);
				break;
				
			case "BECAME_INVISIBLE":
				int angleOfInvisibility = invisibilitySlider.getValue();
				behaviour = new Events(Events.getEventsNum(evName), refDisk.diskName, angleOfInvisibility);
				break;
			}

			this.paintPanel.setCurrentDrawItemToDefault();
			isChosen = true;
			dispose();
		}
	}


	protected Interactions getSelectedInteraction() {
		updateSelectedInteraction();
		return (Interactions)this.behaviour;
	}

	protected Events getSelectedEvent() {
		updateSelectedEvent();
		return (Events)this.behaviour;
	}
}
