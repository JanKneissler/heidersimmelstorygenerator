package storygenerator.menu;

import java.io.Serializable;
import java.util.LinkedList;

import storygenerator.Assignment;
import storygenerator.Events;
import storygenerator.Interactions;
import diskworld.Disk;
import diskworld.extension.Utils;

public class MenuAssignment implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public MenuDisk thisDisk;
	public Events[][] matrix;
	public LinkedList<Interactions> listInteractions;
	
	protected double currentSpeed;
	
	public LinkedList<Interactions> getListInteractions() {
		return this.listInteractions;
	}

	public MenuAssignment(MenuDisk thisDisk, Events[][]matrix, Interactions[] listInteractions) {
		this.thisDisk = thisDisk;
		this.matrix = matrix;
		this.listInteractions = getListInsteadOfArray(listInteractions);
		this.currentSpeed = 0.0;
	}
	
	public MenuAssignment(MenuDisk thisDisk, Events[][]matrix, LinkedList<Interactions> listInteractions) {
		this.thisDisk = thisDisk;
		this.matrix = matrix;
		this.listInteractions = listInteractions;
		this.currentSpeed = 0.0;
	}
	
	public MenuAssignment(Disk thisDisk, Events[][]matrix, Interactions[] listInteractions) {
		this.thisDisk = new MenuDisk(thisDisk);
		this.matrix = matrix;
		this.listInteractions = getListInsteadOfArray(listInteractions);
		this.currentSpeed = 0.0;
	}
	
	public MenuAssignment(Assignment a) {
		this.thisDisk = new MenuDisk(a.thisDisk);
		this.listInteractions = getListInsteadOfArray(a.listInteractions);
		this.currentSpeed = a.currentSpeed;
		
		this.matrix = a.matrix;
		if(a.matrix.length != a.listInteractions.length) {
			System.out.println("Error at MenuAssignment");
		}
		this.matrix = new Events[a.matrix.length][a.matrix.length];
		for(int i=0; i<a.matrix.length; i++) {
			for(int j=0; j<a.matrix[i].length; j++) {
				this.matrix[i][j] = a.matrix[i][j];
			}
		}
	}
	
	public void setCurrentSpeed(double speed) {
		this.currentSpeed = speed;
	}
	
	protected void roundValues(int numDecimalPlaces) {
		thisDisk.radius = Utils.round(thisDisk.radius, 2);
		thisDisk.x = Utils.round(thisDisk.x, 2);
		thisDisk.y = Utils.round(thisDisk.y, 2);
	}
	
	private LinkedList<Interactions> getListInsteadOfArray(Interactions[]iaArray) {
		LinkedList<Interactions> ias = new LinkedList<Interactions>();
		for(int i=0; i<iaArray.length; i++)
			ias.add(iaArray[i]);
		return ias;
	}
	
}
