package storygenerator.menu.eventBoundary;

import javax.swing.JFrame;
import javax.swing.JPanel;

import diskworld.Disk;

public interface EventTransitionTrigger {
	
	public boolean hasOccurred(double currentTime, Disk thisDisk, double momentOfLastEvent);

//	public JFrame getBehaviorChooser();
	
	public JPanel getSettingsPanel();	
}
