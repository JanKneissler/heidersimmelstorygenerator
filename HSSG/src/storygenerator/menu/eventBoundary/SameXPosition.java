package storygenerator.menu.eventBoundary;

import javax.swing.JPanel;

import diskworld.Disk;

public class SameXPosition implements EventTransitionTrigger {

	private Disk referenceDisk;

	public SameXPosition(Disk referenceDisk) {
		this.referenceDisk = referenceDisk;
	}
	
	@Override
	public boolean hasOccurred(double currentTime, Disk thisDisk, double momentOfLastEvent) {
		double referenceHeight = referenceDisk.getX();
		double thisHeight = thisDisk.getX();
		return (Math.abs(referenceHeight - thisHeight) < 0.005);	
	}

//	@Override
//	public JFrame getBehaviorChooser() {
//		String explanation2 = "at the same x position";
//		p = getReferenceDiskCard(listAss, currAss, explanation2, false);
//		p.setName(s);
//		cards.add(p, s);
//		return null;
//	}

	@Override
	public JPanel getSettingsPanel() {
		// TODO Auto-generated method stub
		return null;
	}

}
