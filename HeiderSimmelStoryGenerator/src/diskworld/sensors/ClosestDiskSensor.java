/*******************************************************************************
 *     DiskWorld - a simple 2D physics simulation environment, 
 *     Copyright (C) 2014  Jan Kneissler
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program in the file "License.txt".  
 *     If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package diskworld.sensors;

import java.util.Set;

import diskworld.Disk;
import diskworld.DiskMaterial;
import diskworld.Environment;
import diskworld.linalg2D.AngleUtils;
import diskworld.linalg2D.Utils;

/**
 * Sensor with a cone shaped sensitive area that detects parameters of the closest disk in range.
 * It is possible to specify a set of DiskMaterials that will be ignored when determining the closest
 * Disk. Possible parameters are:
 * - position angle of closest point relative to centre angle of sensitive area
 * - distance to closest point on disk
 * - width angle that the disk occupies within the sensitive area
 * - disk material dependent response
 * The first measurement entry (in component [0]) is always a boolean that indicates if there
 * is one or several disks in the sensitive area (1.0) or none (0.0).
 * 
 * @author Jan
 * 
 */
public class ClosestDiskSensor extends ConeSensor {

	private static final double DEFAULT_MIN_RANGE_RELATIVE = 1.5; // 50% more than disk radius
	public static final String SENSOR_NAME = "DiskDistance";

	private double maxRange;
	private double centerAngle;
	private double openingAngle;
	private final Set<DiskMaterial> invisibleMaterials;
	private int dim, posIndex, distIndex, widthIndex, materialIndex;
	private DiskMaterialResponse materialResponseFunction;

	/**
	 * Constructor for a ClosestDiskSensor
	 * 
	 * @param environment
	 *            environment the sensor lives in
	 * @param centerAngle
	 *            the angle of the centre of the cone
	 * @param openingAngle
	 *            width of the cone
	 * @param minRangeRelative
	 *            minimum distance (inner radius), specified in multiples of the disk radius
	 * @param maxRangeAbsolute
	 *            maximum distance (outer radius), in absolute units
	 * @param invisibleMaterials
	 *            a set of materials that are not detected by the sensor, if null: all disks are detected
	 * @param measurePosition
	 *            indicates if the angular position of the closest point of the detected disk is measured
	 * @param measureDistance
	 *            indicates if the distance to the closest point of the detected disk is measured
	 * @param measureWidth
	 *            indicates if the visible angle the detected disk is measured
	 * @param materialResponse
	 *            function which determines a DiskMaterial dependent response, can be null
	 */
	public ClosestDiskSensor(
			Environment environment,
			double centerAngle,
			double openingAngle,
			double minRangeRelative,
			double maxRangeAbsolute,
			Set<DiskMaterial> invisibleMaterials,
			boolean measurePosition,
			boolean measureDistance,
			boolean measureWidth,
			DiskMaterialResponse materialResponse) {
		super(environment, centerAngle, openingAngle, minRangeRelative, maxRangeAbsolute, SENSOR_NAME);
		this.maxRange = maxRangeAbsolute;
		this.centerAngle = centerAngle;
		this.openingAngle = openingAngle;
		this.invisibleMaterials = invisibleMaterials;
		dim = 1;
		if (measurePosition) {
			posIndex = dim;
			dim++;
		} else {
			posIndex = -1;
		}
		if (measureDistance) {
			distIndex = dim;
			dim++;
		} else {
			distIndex = -1;
		}
		if (measureWidth) {
			widthIndex = dim;
			dim++;
		} else {
			widthIndex = -1;
		}
		materialResponseFunction = materialResponse;
		if (materialResponseFunction != null) {
			materialIndex = dim;
			dim += materialResponseFunction.getDim();
		} else {
			materialIndex = -1;
		}
		addVisualisationOptions();
	}

	/**
	 * Convenience method constructs sensor some parameters set to default, only measuring position angle
	 * 
	 * @param environment
	 *            environment the sensor lives in
	 * @param openingAngle
	 *            width of the cone
	 * @param maxRangeAbsolute
	 *            maximum distance (outer radius), in absolute units
	 * @param invisibleMaterials
	 *            a set of materials that are not detected by the sensor, if null: all disks are detected
	 * @return new ClosestDiskSensor object
	 */
	public static ClosestDiskSensor getPositionAngleSensor(Environment environment, double openingAngle, double maxRangeAbsolute, Set<DiskMaterial> invisibleMaterials) {
		return new ClosestDiskSensor(environment, 0.0, openingAngle, DEFAULT_MIN_RANGE_RELATIVE, maxRangeAbsolute, invisibleMaterials, true, false, false, null);
	}

	/**
	 * Convenience method constructs sensor some parameters set to default, only measuring position angle
	 * 
	 * @param environment
	 *            environment the sensor lives in
	 * @param openingAngle
	 *            width of the cone
	 * @param maxRangeAbsolute
	 *            maximum distance (outer radius), in absolute units
	 * @return new ClosestDiskSensor object
	 */
	public static ClosestDiskSensor getPositionAngleSensor(Environment environment, double openingAngle, double maxRangeAbsolute) {
		return new ClosestDiskSensor(environment, 0.0, openingAngle, DEFAULT_MIN_RANGE_RELATIVE, maxRangeAbsolute, null, true, false, false, null);
	}

	/**
	 * Convenience method constructs sensor some parameters set to default, only measuring distance
	 * 
	 * @param environment
	 *            environment the sensor lives in
	 * @param openingAngle
	 *            width of the cone
	 * @param maxRangeAbsolute
	 *            maximum distance (outer radius), in absolute units
	 * @param invisibleMaterials
	 *            a set of materials that are not detected by the sensor, if null: all disks are detected
	 * @return new ClosestDiskSensor object
	 */
	public static ClosestDiskSensor getDistanceSensor(Environment environment, double openingAngle, double maxRangeAbsolute, Set<DiskMaterial> invisibleMaterials) {
		return new ClosestDiskSensor(environment, 0.0, openingAngle, DEFAULT_MIN_RANGE_RELATIVE, maxRangeAbsolute, invisibleMaterials, false, true, false, null);
	}

	/**
	 * Convenience method constructs sensor some parameters set to default, only measuring distance
	 * 
	 * @param environment
	 *            environment the sensor lives in
	 * @param openingAngle
	 *            width of the cone
	 * @param maxRangeAbsolute
	 *            maximum distance (outer radius), in absolute units
	 * @return new ClosestDiskSensor object
	 */
	public static ClosestDiskSensor getDistanceSensor(Environment environment, double openingAngle, double maxRangeAbsolute) {
		return new ClosestDiskSensor(environment, 0.0, openingAngle, DEFAULT_MIN_RANGE_RELATIVE, maxRangeAbsolute, null, false, true, false, null);
	}

	private void addVisualisationOptions() {
		if (posIndex != -1) {
			variants.add(0, rayTextVisualization(posIndex));
			variants.add(0, rayVisualization(posIndex));
		}
		if (distIndex != -1) {
			variants.add(0, arcTextVisualization(distIndex));
			variants.add(0, arcVisualization(distIndex));
		}
		if ((distIndex != -1) && (posIndex != -1)) {
			variants.add(0, arcVisualization(posIndex, distIndex));
		}
	}

	@Override
	public int getDimension() {
		return dim;
	}

	@Override
	protected void doMeasurement(double measurement[]) {
		double centerx = getDisk().getX();
		double centery = getDisk().getY();
		double direction = getDisk().getAngle() + centerAngle;
		double min = maxRange;
		Disk closestDisk = null;
		for (Disk d : getDisksInShape()) {
			DiskMaterial material = d.getDiskType().getMaterial();
			if ((invisibleMaterials == null) || (!invisibleMaterials.contains(material))) {
				double dx = d.getX() - centerx;
				double dy = d.getY() - centery;
				double dist = Math.sqrt(dx * dx + dy * dy) - d.getRadius();
				if (dist <= min) {
					min = dist;
					closestDisk = d;
				}
			}
		}
		if (closestDisk == null) {
			for (int i = 0; i < measurement.length; i++) {
				measurement[i] = 0.0;
			}
		} else {
			double dx = closestDisk.getX() - centerx;
			double dy = closestDisk.getY() - centery;
			measurement[0] = 1.0;
			if (posIndex != -1) {
				double angle = Math.atan2(dy, dx);
				measurement[posIndex] = Utils.clip_pm1(AngleUtils.mod2PI(angle - direction) / openingAngle * 2);
				// TODO: this is not correct if the center of the disk is outside the cone!
			}
			if (distIndex != -1) {
				double range[] = getShape().referenceValues();
				measurement[distIndex] = Utils.clip_01((min - range[0]) / (range[1] - range[0]));
				// TODO: this is not correct if the center of the disk is outside the cone!
			}
			if (widthIndex != -1) {
				double cdist = Math.sqrt(dx * dx + dy * dy);
				double angle = 2 * Math.atan2(closestDisk.getRadius(), cdist);
				measurement[distIndex] = Utils.clip_01(angle / openingAngle * 2);
				// TODO: this is not correct if the center of the disk is outside the cone!
			}
			if (materialResponseFunction != null) {
				materialResponseFunction.putResponse(closestDisk.getDiskType().getMaterial(), measurement, materialIndex);
			}
		}
	}

	@Override
	public double getRealWorldInterpretation(double measurement[], int index) {
		if (index == distIndex) {
			double range[] = getShape().referenceValues();
			return range[0] + measurement[index] * (range[1] - range[0]);
		}
		if (index == posIndex) {
			return measurement[index] * openingAngle / 2;
		}
		if (index == widthIndex) {
			return measurement[index] * openingAngle / 2;
		}
		return measurement[index];
	}

	@Override
	public String getRealWorldMeaning(int index) {
		if (index == 0) {
			return "disk in range [bool]";
		}
		if (index == distIndex) {
			return "distance to closest point";
		}
		if (index == posIndex) {
			return "relative angle to closest point [rad]";
		}
		if (index == widthIndex) {
			return "visible width angle [rad]";
		}
		return "material response #" + (index - materialIndex);
	}
}
