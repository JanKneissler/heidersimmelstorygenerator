package diskworld.storygenerator;

import java.awt.geom.Line2D;
import java.awt.geom.Line2D.Double;
import java.io.Serializable;
import java.util.LinkedList;

import diskworld.Disk;
import diskworld.linalg2D.Line;
import diskworld.storygeneratorMenu.A_MenuMain;

public class Events extends Behaviour implements Serializable {
	private static final long serialVersionUID = 1L;


	public static final int NUM_OVERALL_EVENTS = 11;

	protected double targetTime;
	protected int angleOfVisibility;
	private LinkedList<Line> listOfWalls;
	private double[] valueHistory = new double[2];
	private int counterOfCallsHelper = 0;
	private double relativeProximity = 0.1;


	public Events(){
		this.name = A_MenuMain.DEFAULT_STRING;
	}

	public Events(int numEvent) {
		this.numOfBehaviour = numEvent;
		this.targetTime = java.lang.Double.POSITIVE_INFINITY;
		this.name = getEventName(numEvent);
	}

	public Events(int numEvent, double targetTime) {
		this.numOfBehaviour = numEvent;
		this.name = getEventName(numEvent);
		this.targetTime = targetTime;
	}

	public Events(int numEvent, Disk referenceDisk) {
		this.numOfBehaviour = numEvent;
		this.targetTime = java.lang.Double.POSITIVE_INFINITY;
		this.referenceDisk = referenceDisk;
		this.nameOfRefDisk = referenceDisk.getName();
		this.name = getEventName(numEvent);
	}

	public Events(int numEvent, Disk referenceDisk, double targetTime) {
		this.numOfBehaviour = numEvent;
		this.referenceDisk = referenceDisk;
		this.nameOfRefDisk = referenceDisk.getName();
		this.name = getEventName(numEvent);
		this.targetTime = targetTime;
	}

	public Events(int numEvent, String nameRefDisk, double aValue) {
		this.numOfBehaviour = numEvent;
		this.nameOfRefDisk = nameRefDisk;
		this.name = getEventName(numEvent);

		if(this.name.equals("BECAME_VISIBLE") || this.name.equals("BECAME_INVISIBLE")) {
			this.angleOfVisibility = (int)aValue;
		} else if(this.name.equals("CLOSE")){
			this.relativeProximity = aValue;
		} else {
			System.err.println("wrong constructor called at Events");
		}
	}

	public Events(int numEvent, String nameRefDisk) {
		this.numOfBehaviour = numEvent;
		this.nameOfRefDisk = nameRefDisk;
		this.name = getEventName(numEvent);
	}

	public boolean hasOccurred(double currentTime, Disk thisDisk, double momentOfLastEvent) {
		int numEvent = this.numOfBehaviour;
		switch(numEvent) {
		case 1:
			return this.always();
		case 2:
			return this.secSinceStart(currentTime);
		case 3:
			return this.periodOfTime(currentTime, momentOfLastEvent);
		case 4:
			return this.touchObject(thisDisk);
		case 5:
			return this.sameXPosition(thisDisk);
		case 6:
			return this.sameYPosition(thisDisk);
		case 7:
			return this.turnIsOver(thisDisk);
		case 8:
			return this.becameVisible(thisDisk);
		case 9:
			return this.becameInvisible(thisDisk);
		case 10:
			return this.close(thisDisk);
		case 11:
			return this.hasStopped();
		}

		//		System.out.println("error in hasOccurred - numEvent does not exist: "+numEvent);
		return false;
	}

	private boolean always() {
		return false;
	}

	private boolean secSinceStart(double currentTime) {
		return (Math.abs(currentTime-this.targetTime) < 0.001);
	}

	private boolean periodOfTime(double currentTime, double momentOfLastEvent) {
		double targetTimePeriod = this.targetTime;  //here: targetTime means targetTimePeriod!
		//		System.out.println("lastEvent: "+this.preciseMomentOfLastEvent+" curr: "+currentTime+" target: "+targetTimePeriod);
		double diffCurrentToLast = Math.abs(currentTime - momentOfLastEvent);
		return (Math.abs(diffCurrentToLast - targetTimePeriod) < 0.05);
	}

	private boolean touchObject(Disk thisDisk) {

		double thisX = thisDisk.getX();
		double thisY = thisDisk.getY();
		double otherX = this.referenceDisk.getX();
		double otherY = this.referenceDisk.getY();

		double distance = Math.sqrt(Math.pow(otherX - thisX, 2)
				+ Math.pow(otherY - thisY, 2)); //pythagoras: compute distance

		if(distance < (0.001 + thisDisk.getRadius() + this.referenceDisk.getRadius())) {
			//			System.out.println("collision! Other disk touched");
			return true;
		} else {
			return false;
		}
	}

	private boolean sameXPosition(Disk thisDisk) {
		double referenceHeight = referenceDisk.getX();
		double thisHeight = thisDisk.getX();
		return (Math.abs(referenceHeight - thisHeight) < 0.005);
	}

	private boolean sameYPosition(Disk thisDisk) {
		double referenceHeight = referenceDisk.getY();
		double thisHeight = thisDisk.getY();
		return (Math.abs(referenceHeight - thisHeight) < 0.02);
	}

	/**
	 * Disk has turned to referenceDisk.
	 * @param thisDisk
	 * @param referenceDisk
	 * @return
	 */
	private boolean turnIsOver(Disk thisDisk) {
		double angle1 = getAngleOfThisDiskAndReferenceDisk(thisDisk);
		double angle2 = thisDisk.getAngle();

		return (Math.abs(angle1 - angle2) < TURN_IS_OVER_MARGIN);
	}

	private boolean becameVisible(Disk thisDisk) {
		
		double angleToRefDisk = getAngleOfThisDiskAndReferenceDisk(thisDisk);
		boolean isInFieldOfVisibility =  (Math.abs(thisDisk.getAngle() - angleToRefDisk) <= 0.5*Math.toRadians(this.angleOfVisibility));

		if(!isInFieldOfVisibility) {
			return false;
		} else { //check for walls
			if (null==listOfWalls || listOfWalls.size()==0) {
				return true;
			} else {
				Line2D.Double lineFromThisDiskToOtherDisk = new Line2D.Double(thisDisk.getX(), thisDisk.getY(), this.referenceDisk.getX(), this.referenceDisk.getY());
				for(Line l : listOfWalls) {
					Line2D.Double l2 = new Line2D.Double(l.getX1()*environmentSize, l.getY1()*environmentSize, l.getX2()*environmentSize, l.getY2()*environmentSize);

					if(l2.intersectsLine(lineFromThisDiskToOtherDisk)) {
						return false;
					}
				}
				return true;
			}
		}
	}

	private boolean becameInvisible(Disk thisDisk) {
		return ! becameVisible(thisDisk);
	}


	private boolean close(Disk thisDisk) {

		// percentage of proximity : if less than, e.g. 5% of environenment, consider
		// it as being close to other disk.

		double thisX = thisDisk.getX();
		double thisY = thisDisk.getY();
		double otherX = this.referenceDisk.getX();
		double otherY = this.referenceDisk.getY();
		
		double distanceBetweenCenterPoints = Math.sqrt(Math.pow(otherX - thisX, 2)
				+ Math.pow(otherY - thisY, 2)); //pythagoras: compute distance
		double distanceBetweenDisks = distanceBetweenCenterPoints - thisDisk.getRadius() - this.referenceDisk.getRadius();
	
		return(distanceBetweenDisks < environmentSize * relativeProximity);
	}

	/**
	 * returns true if the values a timestep before were almost equal to the
	 * current values (position) of the Disk.
	 * Note: A disk that turns but does not move will be regarded as stopping.
	 * @param thisDisk
	 * @return
	 */
	private boolean hasStopped() {
		counterOfCallsHelper++;

		if(counterOfCallsHelper % 10 == 0 ) {

			double[] oldValues = new double[2];
			oldValues[0] = valueHistory[0];
			oldValues[1] = valueHistory[1];

			double[] currentValues = new double[2];

			Disk referenceDisk = this.getReferenceDisk();

			if(null==referenceDisk) {
				return false;
			}

			currentValues[0] = referenceDisk.getX();
			currentValues[1] = referenceDisk.getY();

			//to safe the new values
			valueHistory[0] = referenceDisk.getX();
			valueHistory[1] = referenceDisk.getY();

			final double EPS = 0.00001;
			return  almostEqual(currentValues[0], oldValues[0], EPS) && almostEqual(currentValues[1], oldValues[1], EPS);
		} else {
			return false;
		}
	}

	private static boolean almostEqual(double a, double b, double eps) {
		return Math.abs(a - b) < eps;
	}

	public static String getEventName(int index) {
		switch(index) {
		case 1:
			return "ALWAYS";
		case 2:
			return "SEC_SINCE_START";
		case 3:
			return "PERIOD_OF_TIME";
		case 4:
			return "TOUCH_OBJECT";
		case 5:
			return "SAME_X_POS";
		case 6:
			return "SAME_Y_POS";
		case 7:
			return "TURN_IS_OVER";
		case 8:
			return "BECAME_VISIBLE";
		case 9:
			return "BECAME_INVISIBLE";
		case 10:
			return "CLOSE";
		case 11:
			return "HAS_STOPPED";
		}
		System.out.println("Error in getEventName");
		return "";
	}

	public static int getEventsNum(String eventName) {
		switch(eventName) {
		case "ALWAYS":
			return 1;
		case "SEC_SINCE_START":
			return 2;
		case "PERIOD_OF_TIME":
			return 3;
		case "TOUCH_OBJECT":
			return 4;
		case "SAME_X_POS":
			return 5;
		case "SAME_Y_POS":
			return 6;
		case "TURN_IS_OVER":
			return 7;
		case "BECAME_VISIBLE":
			return 8;
		case "BECAME_INVISIBLE":
			return 9;
		case "CLOSE":
			return 10;
		case "HAS_STOPPED":
			return 11;
		}
		System.out.println("Error in getEventNum");

		final int NOT_SELECTED = -1;
		return NOT_SELECTED;
	}

	/**
	 * method to check whether the construction of an event with number numOfEvent needs
	 * a reference disk for initialization. (used in ChooseEvent to prevent assigning an
	 * Event with a reference Disk to a story with only a single disk)
	 * @param numOfEvent
	 * @return
	 */
	public static boolean referenceDiskNeeded(int numOfEvent) {
		switch(numOfEvent) {
		case 1:
			return false;
		case 2:
			return false;
		case 3:
			return false;
		case 4:
			return true;
		case 5:
			return true;
		case 6:
			return true;
		case 7:
			return true;
		case 8:
			return true;
		case 9:
			return true;
		case 10:
			return true;
		case 11:
			return true;
		}
		//		System.out.println("Error in referenceDiskNeeded - "
		//				+ "numOfEvent:"+numOfEvent+" does not exist");
		return false;
	}

	public static String[] getAllEventNames() {
		String[] eventNames = new String[NUM_OVERALL_EVENTS];
		for(int i=1; i<=NUM_OVERALL_EVENTS; i++) {
			eventNames[i-1] = getEventName(i);
		}
		return eventNames;
	}

	public static String[] getEventNamesWithoutRefDiskNeeded() {
		int numOfEventsNeedingReferenceDisk = 0;
		for(int i=1; i<=NUM_OVERALL_EVENTS; i++) {
			if(!referenceDiskNeeded(i)) {
				numOfEventsNeedingReferenceDisk++;
			}
		}

		String[] eventNames = new String[numOfEventsNeedingReferenceDisk];
		int counter = 0;
		for(int i=1; i<=NUM_OVERALL_EVENTS; i++) {
			if(!referenceDiskNeeded(i)) {
				eventNames[counter] = getEventName(i);
				counter++;
			}
		}		
		return eventNames;
	}

	private double getAngleOfThisDiskAndReferenceDisk(Disk thisDisk) {
		double tx = thisDisk.getX();
		double ty = thisDisk.getY();
		double rx = referenceDisk.getX();
		double ry = referenceDisk.getY();
		double angle1 = Math.atan2((ry - ty), (rx - tx));
		return angle1;
	}

	/**-------------------------------------------------------------------
	 * 						getter + setter 
	 *--------------------------------------------------------------------
	 */

	@Override
	public boolean referenceDiskNeeded() {
		return referenceDiskNeeded(this.numOfBehaviour);
	}

	@Override
	public boolean isEvent() {
		return true;
	}

	@Override
	public void setEnvironmentSize(int size) {
		this.environmentSize = size;
	}

	public double getTargetTimeOfEvent() {
		return this.targetTime;
	}
	
	public double getRelativeProximity() {
		return this.relativeProximity;
	}

	public boolean angleOfVisibilityNeeded() {
		return(this.getName().equals("BECAME_VISIBLE") || this.getName().equals("BECAME_INVISIBLE"));
	}
	
	public boolean relativeProximityNeeded() {
		return(this.getName().equals("CLOSE"));
	}

	public int getAngleOfVisibility() {
		return this.angleOfVisibility;
	}

	public void setWallList(LinkedList<Line> listOfWalls) {
		if(this.angleOfVisibilityNeeded()) {
			this.listOfWalls = listOfWalls;
		} else {
			System.out.println("Error in setWallList");
		}
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

}
