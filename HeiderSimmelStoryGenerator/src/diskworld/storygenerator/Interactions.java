package diskworld.storygenerator;


import java.io.Serializable;

import diskworld.Disk;
import diskworld.linalg2D.Point;
import diskworld.storygeneratorMenu.MenuDisk;
public class Interactions extends Behaviour implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final int NUM_OVERALL_INTERACTIONS = 11;

	private static double speed;

	protected Point[] trajectory;

	protected int numNextPointOnTrajectory;
	private double radiusOfRotation;
	private Point point = new Point(-1, -1); //NOT center of rotation!
	private double impulseSize;
	private double chasingDistance;

	private double angularSpeedWhileTurning;

	private boolean continueTrajectory;

	private long calledLastTimePhysically = System.currentTimeMillis(); //workaround to apply new impulse
	private double impulseX;
	private double impulseY;

	public Interactions(int numInteraction, MenuDisk referenceDisk, double radiusOfRotation) {
		if(numInteraction!=10) {
			System.out.println("Error at Interactions - wrong constructor called");
		} else {
			this.numOfBehaviour = numInteraction;
			this.nameOfRefDisk = referenceDisk.diskName;
			this.name = getInteractionName(numInteraction);
			this.radiusOfRotation = radiusOfRotation;

			initInteractionValues();
		}
	}

	public Interactions(int numInteraction, String referenceDiskName, double value) {
		this.numOfBehaviour = numInteraction;
		this.nameOfRefDisk = referenceDiskName;

		initInteractionValues();

		if(numInteraction == 10) { //ROTATE_AROUND
			this.radiusOfRotation = value;
		} else if(numInteraction == 8) { //TURN_TO
			this.angularSpeedWhileTurning = value;
		} else if(numInteraction == 6) { //CHASE
			this.chasingDistance = value;
		} else {			
			System.err.println("Error at Interactions - wrong constructor called");
		}
	}

	public Interactions(int numInteraction, String referenceDiskName) {
		this.numOfBehaviour = numInteraction;
		this.nameOfRefDisk = referenceDiskName;

		initInteractionValues();
	}
	
	public Interactions(int numInteraction) {
		this.numOfBehaviour = numInteraction;

		initInteractionValues();
	}

	public Interactions(int numInteraction, Point point, double radius) {
		this.numOfBehaviour = numInteraction;
		this.trajectory = new Point[1];
		this.trajectory[0] = point;
		this.radiusOfRotation = radius;

		initInteractionValues();
	}

	public Interactions(int numInteraction, Point[] trajectory, boolean continueTrajectory) {
		this.numOfBehaviour = numInteraction;
		this.trajectory = trajectory;
		this.continueTrajectory = continueTrajectory;

		initInteractionValues();
	}

	public Interactions(int numInteraction, double impulseSizeAkaChasingDistance) {
		this.numOfBehaviour = numInteraction;

		initInteractionValues();
		
		if(this.name.equals("CHASE")) {
			this.chasingDistance = impulseSizeAkaChasingDistance;
		} else if(this.name.equals("PHYSICALLY")) {
			this.impulseSize = impulseSizeAkaChasingDistance;
		} else {
			System.err.println("Error at Interactions - wrong constructor called");
		}
	}

	private void initInteractionValues()
	{
		this.name = getInteractionName(this.numOfBehaviour);
		this.numNextPointOnTrajectory = 0;

	}

	public void reset()
	{
		this.numNextPointOnTrajectory = 0;
	}

	public double[] getTimeStepValues(Disk thisDisk, double currentSpeed) {

		speed = currentSpeed;

		double[] values = new double[3];

		if(this.numOfBehaviour != 1) {
			thisDisk.getDiskComplex().setVelocity(0.0, 0.0);
		}

		switch(this.numOfBehaviour) {
		case 1:
			values = this.physically(thisDisk);
			break;
		case 2:
			values = this.randomly(thisDisk);
			break;
		case 3:
			values = this.trajectory(thisDisk);
			break;
		case 4:
			values = this.rotate(thisDisk);
			break;
		case 5:
			values = this.stop(thisDisk);
			break;
		case 6:
			values = this.chase(thisDisk);
			break;
		case 7:
			values = this.avoid(thisDisk);
			break;
		case 8:
			values = this.turnTo(thisDisk);
			break;
		case 9:
			values = this.avoidAndRotate(thisDisk);
			break;
		case 10:
			values = this.rotateAround(thisDisk);
			break;
		case 11:
			values = this.disappear(thisDisk);
			break;
		}

		return values;
	}


	public double[] physically(Disk thisDisk) {

		//		System.out.println(System.currentTimeMillis() - this.calledLastTimePhysically);
		//		System.out.println("speed: "+thisDisk.getDiskComplex().getSpeedx());

		if(Math.abs(System.currentTimeMillis() - this.calledLastTimePhysically) > 15) {
			double margin = 0.01;
			if( (Math.abs(thisDisk.getDiskComplex().getSpeedx()) < margin) 
					&& Math.abs(thisDisk.getDiskComplex().getSpeedy()) < margin) {

				thisDisk.applyImpulse(impulseX, impulseY);
			}
		}

		this.calledLastTimePhysically = System.currentTimeMillis();

		double[] values = new double[3];

		double newx = thisDisk.getX() + thisDisk.getDiskComplex().getSpeedx() * speed;
		double newy = thisDisk.getY() + thisDisk.getDiskComplex().getSpeedy() * speed;
		double newangle = getNewAngle(thisDisk, newx, newy);

		values[0] = newx;
		values[1] = newy;
		values[2] = newangle;

		return values;
	}

	/**
	 * thisDisk will go to a randomly chosen Point within the environment.
	 * Keep in mind that it might run into walls/other disks.
	 * @param thisDisk
	 * @return
	 */
	public double[] randomly(Disk thisDisk) {
		Point me = new Point(thisDisk.getX(), thisDisk.getY());

		double add = 0.0;

		if(me.distance(this.point) < 0.01 || this.point.x == -1) {
			double radius = thisDisk.getRadius();
			double x, y;
			do {
				x = Math.random()*environmentSize;
				y = Math.random()*environmentSize;
			} while (x < radius+add || y < radius+add || x+add > environmentSize - radius || y+add > environmentSize - radius);

			this.point.x = x;
			this.point.y = y;
		}
		return this.goToPoint(thisDisk, this.point);
	}

	public double[] trajectory(Disk thisDisk) {
		double targetx = trajectory[trajectory.length-1].x*environmentSize;
		double targety = trajectory[trajectory.length-1].y*environmentSize;
		
		if(numNextPointOnTrajectory < trajectory.length) {
			targetx = trajectory[numNextPointOnTrajectory].x*environmentSize;
			targety = trajectory[numNextPointOnTrajectory].y*environmentSize;
		}
		double positionReachedMargin = 0.005 * this.environmentSize;

		if( (Math.abs(thisDisk.getX() - targetx) < positionReachedMargin) && (Math.abs(thisDisk.getY() - targety) < positionReachedMargin))
		{
			if((numNextPointOnTrajectory+1) < trajectory.length) {
				numNextPointOnTrajectory++;
				targetx = trajectory[numNextPointOnTrajectory].x*environmentSize;
				targety = trajectory[numNextPointOnTrajectory].y*environmentSize;
				return this.goToPoint(thisDisk, targetx, targety);
			} else { //end of trajectory is reached.
				if(continueTrajectory && trajectory.length>1) {
					numNextPointOnTrajectory = 0;
					targetx = trajectory[numNextPointOnTrajectory].x*environmentSize;
					targety = trajectory[numNextPointOnTrajectory].y*environmentSize;
					return this.goToPoint(thisDisk, targetx, targety);
				} else {
					// numNextPointOnTrajectory = trajectory.length;
					return this.stop(thisDisk);
				}
			}
		} else {
			if(numNextPointOnTrajectory == trajectory.length)
				numNextPointOnTrajectory = 0;
			return this.goToPoint(thisDisk, targetx, targety);
		}
		
	}

	/**
	 * Disk will go the straight way to the circular edge.
	 * The centerpoint of the circle needs to be initialized in the constructor of the Interaction,
	 * same for the desired radius.
	 * The Disk will then follow the circular edge counterclockwise.
	 * @param thisDisk
	 * @return
	 */
	public double[] rotate(Disk thisDisk) {	
		Point M = new Point(this.trajectory[0].x*environmentSize, this.trajectory[0].y*environmentSize);
		return this.rotateHelper(thisDisk, M);
	}

	public double[] stop(Disk thisDisk) {
		double[] values = new double[3];
		values[0] = thisDisk.getX();
		values[1] = thisDisk.getY();
		values[2] = thisDisk.getAngle();
		return values;
	}


	public double[] chase(Disk thisDisk) {
		double[] values = new double[3];
		double newx;
		double newy;

		double thisx = thisDisk.getX();
		double thisy = thisDisk.getY();
		double thisRadius = thisDisk.getRadius();

		double targetx = referenceDisk.getX();
		double targety = referenceDisk.getY();
		double otherRadius = referenceDisk.getRadius();

		final double MAXIMUM_CHASING_DISTANCE_IN_PERCENT = 0.5;
		double safetyDistance = MAXIMUM_CHASING_DISTANCE_IN_PERCENT * this.environmentSize * this.getChasingDistance();

		double distance = Math.sqrt(Math.pow(targetx - thisx, 2) + Math.pow(targety - thisy, 2)); //pythagoras: compute distance

		double necessaryTimeSteps = distance / speed;
		double newangle;

		if(distance > thisRadius + otherRadius + safetyDistance) {
			newx = thisx + (targetx - thisx) / necessaryTimeSteps;
			newy = thisy + (targety - thisy) / necessaryTimeSteps;
			newangle = Math.atan2((targety-thisy),(targetx - thisx));
		} else {
			newx = thisx;
			newy = thisy;
			newangle = thisDisk.getAngle();
		}
			
		values[0] = newx;
		values[1] = newy;
		values[2] = newangle;

		return values;
	}


	public double[] avoid(Disk thisDisk) {

		double[] values;
		double targetx, targety;

		double distanceToBorders = 2 * thisDisk.getRadius();
		distanceToBorders = distanceToBorders<0.05?0.05:distanceToBorders;
		double minimalDistanceToOtherDisk = environmentSize / 3; //parametrization possible
		double minDistToChangeBehavior = (environmentSize + environmentSize)/30;

		if(environmentSize < 2 * distanceToBorders || environmentSize < 2 * distanceToBorders) {
			System.out.println("Error: borders too small.");
		}

		Point enemy = new Point(referenceDisk.getX(), referenceDisk.getY());
		Point me = new Point(thisDisk.getX(), thisDisk.getY());

		double distToEnemy = me.distance(enemy);
		if(distToEnemy >= minimalDistanceToOtherDisk) {
			//			Point p = new Point(environmentX/2, environmentY/2);
			//			values = this.goToPoint(thisDisk, p);
			values = this.stop(thisDisk);
		} else if(distToEnemy <= minDistToChangeBehavior) {
			values = this.physically(thisDisk);
		} else {


			/* visualization: A, B, C and D...
			 * 
			  ___________________
			  |					 |
			  |		D--C		 |
			  |		|  |		 | <-Border
			  |		A--B..dist...|
			  |					 |
			  |__________________|					
			 */

			Point A = new Point(distanceToBorders, distanceToBorders);
			Point B = new Point(environmentSize - distanceToBorders, distanceToBorders);
			Point C = new Point(environmentSize - distanceToBorders, environmentSize - distanceToBorders);
			Point D = new Point(distanceToBorders, environmentSize - distanceToBorders);


			// if you are in the corner: go to next corner(counterclockwise)
			/*
			if(distToEnemy < envx / 2) {
				if(this.corner.x == -1 && this.corner.y == -1) {
					double minDistToCorner = envx / 10; //param
					if(me.distance(A) < minDistToCorner)
						this.corner = B;
					else if(me.distance(B) < minDistToCorner)
						this.corner = C;
					else if(me.distance(C) < minDistToCorner)
						this.corner = D;
					else if(me.distance(D) < minDistToCorner)
						this.corner = A;
				} else {
					return goToPoint(thisDisk, this.corner);
				}
			} else {
				this.corner = new Point(-1, -1);
			}
			 */

			Point iAB = Point.getIntersectionPoint(enemy, me, A, B);
			Point iBC = Point.getIntersectionPoint(enemy, me, B, C);
			Point iCD = Point.getIntersectionPoint(enemy, me, C, D);
			Point iDA = Point.getIntersectionPoint(enemy, me, D, A);

			Point possible1;
			Point possible2;

			boolean bAB = true;
			boolean bBC = true;
			boolean bCD = true;
			boolean bDA = true;

			if(enemy.y >= me.y)
				bCD = false;
			else
				bAB = false;
			if(enemy.x >= me.x)
				bBC = false;
			else
				bDA = false;

			if(bAB && bBC) {
				possible1 = iAB;
				possible2 = iBC;
			} else if(bAB && bCD) {
				possible1 = iAB;
				possible2 = iCD;
			} else if(bAB && bDA) {
				possible1 = iAB;
				possible2 = iDA;
			} else if(bBC && bCD) {
				possible1 = iBC;
				possible2 = iCD;
			} else if(bBC && bDA) {
				possible1 = iBC;
				possible2 = iDA;
			} else {
				if(!bCD || !bDA)
					System.out.println("Error at Interactions.avoid");
				possible1 = iCD;
				possible2 = iDA;
			}

			//			System.out.println(bAB+" "+bBC+" "+bCD+" "+bDA);

			double distance1 = me.distance(possible1);
			double distance2 = me.distance(possible2);

			if(possible1.x < distanceToBorders || possible1.x > environmentSize-distanceToBorders || possible1.y < distanceToBorders || possible1.y > environmentSize - distanceToBorders) {
				targetx = possible2.x;
				targety = possible2.y;
			} else if(possible2.x < distanceToBorders || possible2.x > environmentSize-distanceToBorders || possible2.y < distanceToBorders || possible2.y > environmentSize - distanceToBorders) {
				targetx = possible1.x;
				targety = possible1.y;
			} else {
				if(distance1 < distance2) {
					targetx = possible1.x;
					targety = possible1.y;
				} else {
					targetx = possible2.x;
					targety = possible2.y;
				}
			}
			if(targetx < distanceToBorders || targety < distanceToBorders || targetx > environmentSize - distanceToBorders || targety > environmentSize - distanceToBorders) {
				return this.stop(thisDisk);
			}

			values = this.goToPoint(thisDisk, targetx, targety);
		}
		return values;
	}

	public double[] turnTo(Disk thisDisk) {

		double[] values = new double[4];
		double thisX = thisDisk.getX();
		double thisY = thisDisk.getY();
		double rx = referenceDisk.getX();
		double ry = referenceDisk.getY();

		double angle1 = convertTo2Pi(Math.atan2((ry - thisY), (rx - thisX)));

		double angle2 = thisDisk.getOrientation();

		if(Math.abs(angle1 - angle2) < TURN_IS_OVER_MARGIN) { 
			values[2] = thisDisk.getAngle();
		} else {
			if(Math.abs(angle2 - angle1) <= Math.PI) {
				values[2] = convertToPMPi(angle2 + (angle2 > angle1? -angularSpeedWhileTurning : angularSpeedWhileTurning));
			} else {
				values[2] = convertToPMPi(angle2 + (angle2 > angle1?  angularSpeedWhileTurning : -angularSpeedWhileTurning));
			}
		}
		values[0] = thisX;
		values[1] = thisY;
		return values;
	}


	private static double convertTo2Pi(double angle) {
		if(angle >= 0) return angle;
		else return 2*Math.PI + angle;
	}

	private static double convertToPMPi(double angle) {
		if(angle <= Math.PI) {
			return angle;
		} else {
			return angle - 2*Math.PI;
		}
	}


	private double[] avoidAndRotate(Disk thisDisk) {
		Point Center = new Point(environmentSize/2, environmentSize/2);
		Point me = new Point(thisDisk.getX(), thisDisk.getY());

		double radius = 0.4;

		if(me.distance(Center) > radius*environmentSize) {
			this.radiusOfRotation = radius;
			return rotateHelper(thisDisk, Center);
		} else {
			return avoid(thisDisk);
		}
	}


	/**
	 * Disk will rotate similar to Interactions.rotate but it will rotate around the referenceDisk.
	 * @param thisDisk
	 * @param referenceDisk
	 * @return
	 */
	public double[] rotateAround(Disk thisDisk) {
		Point M = new Point(referenceDisk.getX(), referenceDisk.getY());
		return this.rotateHelper(thisDisk, M);
	}

	public double[] rotateHelper(Disk thisDisk, Point CenterOfRotation) {
		Point me = new Point(thisDisk.getX(), thisDisk.getY());
		double radiusOfRotation = this.radiusOfRotation * environmentSize;

		double margin = 0.01*environmentSize;

		if((me.distance(CenterOfRotation)-radiusOfRotation) > margin) { //outside -> go in direction of the center of circle
			//						System.out.println("outside");
			return this.goToPoint(thisDisk, CenterOfRotation);
		}

		if(me.distance(CenterOfRotation)-radiusOfRotation < (-1)*margin) { //inside -> go in direction of the outside
			//			System.out.println("inside");

			if(me.x == CenterOfRotation.x && me.y == CenterOfRotation.y) { // if in the middle -> go righthand to the outside
				return this.goToPoint(thisDisk, CenterOfRotation.x + 1.0, CenterOfRotation.y);
			} else { // inside the circle -> go on a straight line to the outside
				double vectorX = me.x - CenterOfRotation.x;
				double vectorY = me.y - CenterOfRotation.y;
				Point pointOut = new Point(me.x + vectorX, me.y + vectorY);
				return this.goToPoint(thisDisk, pointOut);
			}
		}

		//		 perfect: thisDisk has the right distance to the center of rotation.
		//		System.out.println("rotate");
		double speedOfRotation = .5 * speed / radiusOfRotation;
		double t = (Math.atan2((me.y - CenterOfRotation.y), (me.x - CenterOfRotation.x))) + speedOfRotation;	
		double x = (CenterOfRotation.x+radiusOfRotation*Math.cos(t));
		double y = (CenterOfRotation.y+radiusOfRotation*Math.sin(t));
		double[]values = new double[3];
		values[0] = x;
		values[1] = y;

		double oppositeSideLength = (me.y - CenterOfRotation.y);
		double adjacentSideLength = (me.x - CenterOfRotation.x);

		double alpha = Math.atan2(oppositeSideLength, adjacentSideLength);

		if(Math.PI/2 < alpha && alpha < Math.PI) {
			values[2] = -3*Math.PI/2 + alpha;
		} else {
			values[2] = alpha + Math.toRadians(90);
		}

		return values;
	}


	public double[]disappear(Disk thisDisk) {
		this.point = new Point(thisDisk.getX(), thisDisk.getY());
		double[]values = new double[3];
		values[0] = environmentSize * 2;
		values[1] = environmentSize * 2;
		values[2] = thisDisk.getAngle();
		return values;
	}

	private double[] goToPoint(Disk thisDisk, Point p) { return this.goToPoint(thisDisk, p.x, p.y); }

	private double[] goToPoint(Disk thisDisk, double targetx, double targety) {
		double[] values = new double[3];
		double newx;
		double newy;

		double thisx = thisDisk.getX();
		double thisy = thisDisk.getY();

		double distance = Math.sqrt(Math.pow(targetx - thisx, 2) + Math.pow(targety - thisy, 2)); //pythagoras: compute distance
		double necessaryTimeSteps = distance / speed;

		newx = thisx + (targetx - thisx) / necessaryTimeSteps;
		newy = thisy + (targety - thisy) / necessaryTimeSteps;

		double newangle = getNewAngle(thisDisk, targetx, targety);

		values[0] = newx;
		values[1] = newy;
		values[2] = newangle;
		return values;
	}

	private double getNewAngle(Disk thisDisk, double targetx, double targety) {
		double thisx = thisDisk.getX();
		double thisy = thisDisk.getY();
		double newangle;
		final double MARGIN = 0.005;

		if(Math.abs(targetx - thisx) < MARGIN && Math.abs(targety - thisy) < MARGIN) {
			newangle = thisDisk.getAngle();
		} else {
			newangle = Math.atan2((targety-thisy), (targetx-thisx));
		}
		return newangle;
	}


	/**
	 * method to check whether the construction of an event with number numOfEvent needs
	 * a reference disk for initialization. (used in ChooseEvent to prevent assigning an
	 * Event with a reference Disk to a story with only a single disk)
	 * @param numOfEvent
	 * @return
	 */
	public static boolean referenceDiskNeeded(int numOfIA) {
		switch(numOfIA) {
		case 1:
			return false;
		case 2:
			return false;
		case 3:
			return false;
		case 4:
			return false;
		case 5:
			return false;
		case 6:
			return true;
		case 7:
			return true;
		case 8:
			return true;
		case 9:
			return true;
		case 10:
			return true;
		case 11:
			return false;
		}
		System.out.println("Error in referenceDiskNeeded - numOfIA does not exist");
		return false;
	}

	public static boolean referenceDiskNeeded(String name) {
		switch(name) {
		case "PHYSICALLY":
			return false;
		case "RANDOMLY":
			return false;
		case "TRAJECTORY":
			return false;
		case "ROTATE":
			return false;
		case "STOP":
			return false;
		case "CHASE":
			return true;
		case "AVOID":
			return true;
		case "TURN_T0":
			return true;
		case "AVOID_AND_ROTATE":
			return true;
		case "ROTATE_AROUND":
			return true;
		case "DISAPPEAR":
			return false;
		}
		System.out.println("Error in Interactions.getInteractionNum");
		return false;
	}


	public static boolean radiusNeeded(String name) {
		switch(name) {
		case "PHYSICALLY":
			return false;
		case "RANDOMLY":
			return false;
		case "TRAJECTORY":
			return false;
		case "ROTATE":
			return true;
		case "STOP":
			return false;
		case "CHASE":
			return false;
		case "AVOID":
			return false;
		case "TURN_T0":
			return false;
		case "AVOID_AND_ROTATE":
			return false;
		case "ROTATE_AROUND":
			return true;
		case "DISAPPEAR":
			return false;
		}
		System.out.println("Error in Interactions.getInteractionNum");
		return false;
	}


	public static String getInteractionName(int index) {
		switch(index) {
		case 1:
			return "PHYSICALLY";
		case 2:
			return "RANDOMLY";
		case 3:
			return "TRAJECTORY";
		case 4:
			return "ROTATE";
		case 5:
			return "STOP";
		case 6:
			return "CHASE";
		case 7:
			return "AVOID";
		case 8:
			return "TURN_T0";
		case 9:
			return "AVOID_AND_ROTATE";
		case 10:
			return "ROTATE_AROUND";
		case 11:
			return "DISAPPEAR";
		}
		System.out.println("Error in getInteractionName");
		return "";
	}

	public static int getInteractionsNum(String name) {
		switch(name) {
		case "PHYSICALLY":
			return 1;
		case "RANDOMLY":
			return 2;
		case "TRAJECTORY":
			return 3;
		case "ROTATE":
			return 4;
		case "STOP":
			return 5;
		case "CHASE":
			return 6;
		case "AVOID":
			return 7;
		case "TURN_T0":
			return 8;
		case "AVOID_AND_ROTATE":
			return 9;
		case "ROTATE_AROUND":
			return 10;
		case "DISAPPEAR":
			return 11;
		}
		System.out.println("Error in Interactions.getInteractionNum");
		return -1;
	}

	public boolean radiusNeeded() {
		switch(this.numOfBehaviour) {
		case 1:
			return false;
		case 2:
			return false;
		case 3:
			return false;
		case 4:
			return true;
		case 5:
			return false;
		case 6:
			return false;
		case 7:
			return false;
		case 8:
			return false;
		case 9:
			return false;
		case 10:
			return true;
		case 11:
			return false;
		}
		return false;
	}

	@Override
	public boolean referenceDiskNeeded() {
		return referenceDiskNeeded(this.numOfBehaviour);
	}

	public Point getPoint() {
		return this.point;
	}

	public double getRadiusOfRotation() {
		return this.radiusOfRotation;
	}

	public Point getCenterOfRotationPoint() {
		return this.trajectory[0];
	}

	@Override
	public boolean isEvent() {
		return false;
	}


	@Override
	public void setEnvironmentSize(int size) {
		this.environmentSize = size;
	}

	public Point[] getTrajectory() {
		return this.trajectory;
	}

	public boolean doContinueTrajectory() {
		return this.continueTrajectory;
	}

	public boolean impulseNeeded() {
		return(this.getName().equals("PHYSICALLY"));
	}

	public double getImpulseSize() {
		return this.impulseSize;
	}

	public boolean angularSpeedNeeded() {
		return(this.getNumOfBehavoiur()==8); //TURN_TO
	}

	public double getAngularSpeed() {
		return this.angularSpeedWhileTurning;
	}

	public boolean chasingDistanceNeeded() {
		return(this.getNumOfBehavoiur()==6); //CHASE
	}

	public double getChasingDistance() {
		return this.chasingDistance;
	}


	public void setImpulse(double impulseX, double impulseY) {
		this.impulseX = impulseX;
		this.impulseY = impulseY;
	}

}
