package diskworld.storygenerator;

import java.io.Serializable;
import java.util.LinkedList;

import diskworld.linalg2D.Line;
import diskworld.storygeneratorMenu.MenuAssignment;

public class Story implements Serializable {

	private static final long serialVersionUID = 1L;
	private LinkedList<MenuAssignment> listOfMenuAsssignments;
	private LinkedList<Line> listOfWalls;
	private String storyname;	
	private boolean showVisualization;

//	public Story(LinkedList<MenuAssignment> listAss, String storyname) {
//		this.listOfMenuAsssignments = listAss;
//		this.storyname = storyname;
//		this.addValuesToEventsMatrix();
//	}
	



	public Story(LinkedList<MenuAssignment> listAss, String storyname, LinkedList<Line> listOfWalls,
			boolean showVisualization) {
		this.listOfMenuAsssignments = listAss;
		this.storyname = storyname;
		this.addValuesToEventsMatrix();
		this.listOfWalls = listOfWalls;
		this.showVisualization = showVisualization;
	}

	public void addValuesToEventsMatrix() {
		for(int i=0; i<this.listOfMenuAsssignments.size(); i++) {
			for(int j=0; j<this.listOfMenuAsssignments.get(i).matrix.length; j++) {
				for(int k=0; k<this.listOfMenuAsssignments.get(i).matrix[j].length; k++) {
					if(this.listOfMenuAsssignments.get(i).matrix[j][k]==null) {
						this.listOfMenuAsssignments.get(i).matrix[j][k] = new Events();
					}
				}
			}
		}
	}
	
	public void printStorySummary() {
		System.out.println("Summary of: "+this.getStoryname());
		System.out.println("---------------------------");
		System.out.println();
		
		for(MenuAssignment ass : this.getListOfMenuAsssignments()) {
			System.out.println("disk: "+ass.thisDisk.diskName);
			for(Behaviour i : ass.getListInteractions()) {
				System.out.println("ia: "+i.getName());
				if(i.referenceDiskNeeded()) {
					System.out.println("   -> ref disk:"
							+ " "+i.getMenuReferenceDiskName());
				}
			}
			for(int i=0; i<ass.matrix.length; i++) {
				for(int j=0; j<ass.matrix[i].length; j++) {
					System.out.println("e: "+ass.matrix[i][j].getName());
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	public String getStoryname() {
		return this.storyname;
	}

	public LinkedList<MenuAssignment> getListOfMenuAsssignments() {
		return this.listOfMenuAsssignments;
	}
	
	public LinkedList<Line> getListOfWalls() {
		return listOfWalls;
	}
	
	public boolean getShowVisualization() {
		return this.showVisualization;
	}
	
}
