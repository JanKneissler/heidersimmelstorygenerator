package diskworld.storygenerator;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;

import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.actuators.Teleporter;
import diskworld.demos.DemoLauncher;
import diskworld.demos.DemoLauncher.Demo;
import diskworld.environment.AgentMapping;
import diskworld.environment.FloorCellType;
import diskworld.environment.Wall;
import diskworld.interfaces.AgentController;
import diskworld.linalg2D.Line;
import diskworld.linalg2D.Point;
import diskworld.storygeneratorMenu.A_MenuMain;
import diskworld.storygeneratorMenu.FileChooser;
import diskworld.storygeneratorMenu.MenuAssignment;
import diskworld.storygeneratorMenu.MenuDisk;
import diskworld.visualization.CircleDiskSymbol;
import diskworld.visualization.PolygonDiskSymbol;
import diskworld.visualization.VisualizationOption;
import diskworld.visualization.VisualizationOptions;
import diskworld.visualization.VisualizationSettings;

public class StoryReadyToPlay implements Demo {

	static JFrame frame = new JFrame();
	public static Environment env;
	static List<Disk[]> mergeList = new LinkedList<Disk[]>();

	private static int NUM_AGENTS;
	private static Assignment[] story;
	private LinkedList<DiskComplex> allAgentComplex = new LinkedList<DiskComplex>();

	private static final boolean DEBUG = false;
	private static Random rand;

	private Story menuStory;

	private static final boolean CHOOSE_STORY = false;

	public static void main(String[] args) {
		if(CHOOSE_STORY) {
			FileChooser chooser = new FileChooser();

			try{
				while(!chooser.isChosen) {
					Thread.sleep(1);
				}
				Story myStory = chooser.myChosenStory;
				DemoLauncher.runDemo(new StoryReadyToPlay(myStory));

			} catch(InterruptedException e) {
			}
		} else {
			DemoLauncher.runDemo(new StoryReadyToPlay(SampleStoryForMenuUse.getSampleStoryUsingOnlyMenuDisks()));
		}
	}



	public StoryReadyToPlay(Story menuStory) {
		rand = new Random(103);
		this.menuStory = menuStory;
	}

	@Override
	public Environment getEnvironment() {
		Environment environment = convertToNormalStory();
		env = environment;
		return env;
	}

	@Override
	public boolean adaptVisualisationSettings(VisualizationSettings settings) {
		//here we can change the default settings for e.g. grid visualization, actuator,...
		settings.getColorScheme().wallColor = Color.white;
		settings.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_GRID).setEnabled(false);
		
		if(! menuStory.getShowVisualization()) {
			VisualizationOption opt = settings.getOptions().getOption(VisualizationOptions.GROUP_ACTUATORS, Teleporter.ACTUATOR_NAME);
			if (opt != null) {
				opt.setEnabled(false);
				return true;
			}
		} else {
			VisualizationOption opt = settings.getOptions().getOption(VisualizationOptions.GROUP_ACTUATORS, Teleporter.ACTUATOR_NAME);
			if (opt != null) {
				opt.setEnabled(true);
				return true;
			}
		}
		
		return false;
	}

	@Override
	public AgentMapping[] getAgentMappings() {

		AgentMapping[] mapping = new AgentMapping[NUM_AGENTS];

		int i=0;
		for(DiskComplex dc : allAgentComplex) {
			AgentMapping map = new AgentMapping(dc, story[i]);
			mapping[i] = map;
			i++;
		}
		return mapping;
	}

	@Override
	public AgentController[] getControllers() {
		AgentController controller = new AgentController() {
			@Override
			public void doTimeStep(double[] sensorValues, double[] actuatorValues) {
				// sensorValues has dim 2 (disk seen, angle to closest disk in view)
				System.out.println("DemoStories.getControllers().doTimeStep called");
				actuatorValues[0] = 5.0;
				actuatorValues[1] = 5.0;
				actuatorValues[2] = 4.7298;
			}

			@Override
			public void doTimeStep(Assignment assignment,
					double[] actuatorValues) {
				double[] values;

				values = assignment.getTimeStepValues(env.getTime());
				actuatorValues[0] = 1.0; //always activated
				actuatorValues[1] = values[0] / env.getMaxX();
				actuatorValues[2] = values[1] / env.getMaxY();
				actuatorValues[3] = values[2] / Math.PI;
				if(DEBUG) {
					System.out.println("values 0 " + actuatorValues[0]);
					System.out.println("values 1 " + actuatorValues[1]);
					System.out.println("values 2 " + actuatorValues[2]);
					System.out.println("values 3 " + actuatorValues[3]);
				}
			}

			@Override
			public boolean hasAssignment() {
				return true;
			}
		};
		AgentController[] cont = new AgentController[NUM_AGENTS];
		for (int i = 0; i < cont.length; i++) {
			cont[i] = controller;
		}
		return cont;
	}

	public Environment convertToNormalStory() {
		LinkedList<MenuAssignment> menuAssignments = menuStory.getListOfMenuAsssignments();

		NUM_AGENTS = menuAssignments.size();
		story = new Assignment[NUM_AGENTS];

		final int sizeOfEnvironment = A_MenuMain.SIZE_OF_ENVIRONMENT;

		Environment env = new Environment(sizeOfEnvironment, sizeOfEnvironment, getWalls(sizeOfEnvironment));
		env.getFloor().fill(FloorCellType.EMPTY);

		//Disk symbols
		PolygonDiskSymbol diskSymbolTrianlge = PolygonDiskSymbol.getSharpTriangleSymbol(0.7D);
		Teleporter teleporterWithVisualization = new Teleporter(env, false, 0.5, 0.0, 0.0, 1.0, diskSymbolTrianlge);

		DiskType agentDrive;

		Disk[] diskArray = new Disk[NUM_AGENTS];
		int diskCounter1=0;
		for(MenuAssignment menuAssignment : menuAssignments) {
			agentDrive = new DiskType(DiskMaterial.METAL.withColor(menuAssignment.thisDisk.col), teleporterWithVisualization);
			MenuDisk menuDisk = menuAssignment.thisDisk;
			Disk disk = env.newRootDisk(menuDisk.x*sizeOfEnvironment, menuDisk.y*sizeOfEnvironment, menuDisk.radius*sizeOfEnvironment, agentDrive);
			
			diskArray[diskCounter1] = disk;
			diskCounter1++;
		}

		int diskCounter2 = 0;
		for(MenuAssignment menuAssignment : menuAssignments) {
			Interactions[] listIAPerDisk = new Interactions[menuAssignment.listInteractions.size()];
			int interactionsCounter=0;
			for(Interactions ia : menuAssignment.listInteractions) {
				listIAPerDisk[interactionsCounter] = ia;
				if(ia.referenceDiskNeeded()) {
					Disk d = getRefDiskFromName(menuAssignments, diskArray, ia);
					listIAPerDisk[interactionsCounter].setReferenceDisk(d);
				}
				if(ia.impulseNeeded()) {
					double impulseSize = ia.getImpulseSize();
					double impulseX = getRandomImpulseDiskMassEtcConsidered(sizeOfEnvironment, sizeOfEnvironment, diskArray[diskCounter2].getMass(), impulseSize);
					double impulseY = getRandomImpulseDiskMassEtcConsidered(sizeOfEnvironment, sizeOfEnvironment, diskArray[diskCounter2].getMass(), impulseSize);

					ia.setImpulse(impulseX, impulseY);
				}
				listIAPerDisk[interactionsCounter].setEnvironmentSize(sizeOfEnvironment);
				interactionsCounter++;

			}
			Events[][] newEventsMatrix = new Events[menuAssignment.matrix.length][menuAssignment.matrix.length];
			for(int i=0; i<newEventsMatrix.length; i++) {
				for(int j=0; j<newEventsMatrix[i].length; j++) {
					newEventsMatrix[i][j] = menuAssignment.matrix[i][j];
					if(newEventsMatrix[i][j].referenceDiskNeeded()) {
					
						Disk d = getRefDiskFromName(menuAssignments, diskArray, menuAssignment.matrix[i][j]);
						newEventsMatrix[i][j].setReferenceDisk(d);
					}
					
					if(newEventsMatrix[i][j].angleOfVisibilityNeeded()) {
						newEventsMatrix[i][j].setWallList(menuStory.getListOfWalls());
					}
				}
			}

			story[diskCounter2] = new Assignment(diskArray[diskCounter2], menuAssignment.matrix, listIAPerDisk);
			allAgentComplex.add(diskArray[diskCounter2].getDiskComplex());
			story[diskCounter2].setSpeed(0.015);

			diskCounter2++;
		}
		return env;
	}

	private LinkedList<Wall> getWalls(double screenSize) {
		LinkedList<Wall> walls = new LinkedList<Wall>();
		for(Line line : menuStory.getListOfWalls()) {
			Line lineOnScreen = new Line(line.getX1()*screenSize, line.getY1()*screenSize,
					line.getX2()*screenSize, line.getY2()*screenSize);
			walls.add(new Wall(lineOnScreen, 0.01*screenSize));
		}
		
		//add bounding walls
		int SIZE = A_MenuMain.SIZE_OF_ENVIRONMENT;
		walls.add(new Wall(new Line(new Point(0, 0), new Point(SIZE, 0)), 0.01));
		walls.add(new Wall(new Line(new Point(SIZE, 0), new Point(SIZE, SIZE)), 0.01));
		walls.add(new Wall(new Line(new Point(SIZE, SIZE), new Point(0, SIZE)), 0.01));
		walls.add(new Wall(new Line(new Point(0, SIZE), new Point(0, 0)), 0.01));
		
		return walls;
	}


	private Disk getRefDiskFromName(
			LinkedList<MenuAssignment> menuAssignments,
			Disk[] diskArray,
			Behaviour behaviour) {

		int z=0;
		for(MenuAssignment menuAss : menuAssignments) {
			if(menuAss.thisDisk.diskName.equals(behaviour.getMenuReferenceDiskName())){
				return diskArray[z];
			}
			z++;
		}
		throw new IllegalArgumentException();
	}


	@Override
	public long getMiliSecondsPerTimeStep() {
		return 5;
	}

	@Override
	public String getTitle() {
		return menuStory.getStoryname();
	}

	public static LinkedList<Wall> getWalls(boolean insidewall, int numWalls) {
		LinkedList<Wall> walls = new LinkedList<Wall>();
		walls.add(new Wall(new Line(new Point(0, 0), new Point(1, 0)), 0.01)); // these 4 walls are the outside walls of the frame
		walls.add(new Wall(new Line(new Point(1, 0), new Point(1, 1)), 0.01));
		walls.add(new Wall(new Line(new Point(1, 1), new Point(0, 1)), 0.01));
		walls.add(new Wall(new Line(new Point(0, 1), new Point(0, 0)), 0.01));
		if (insidewall)
			switch(numWalls) {
			case 1:
				walls.add(new Wall(new Line(new Point(0.49, 0.5), new Point(0.51, 0.5)), 0.6));
				break;
			case 2:
				walls.add(new Wall(new Line(new Point(0.25, 0.25), new Point(0.25, 0.75)), 0.1));
				walls.add(new Wall(new Line(new Point(0.25, 0.75), new Point(0.75, 0.75)), 0.1));
				walls.add(new Wall(new Line(new Point(0.75, 0.0), new Point(0.85, 0.25)), 0.5));
				break;
			case 3:
				walls.add(new Wall(new Line(new Point(0.0, 0.5), new Point(0.4, 0.5)), 0.1));
				walls.add(new Wall(new Line(new Point(1.0, 0.5), new Point(0.6, 0.5)), 0.1));
				break;
			}
		return walls;
	}

	/**
	 * returns an impulse, positive or negative.
	 * @return
	 */
	public static double getRandomImpulseDiskMassEtcConsidered(double sizeOfEnvironmentX, double sizeOfEnvironmentY, double diskMass, double impulseSize) {
		double sign = rand.nextDouble() > 0.5 ? -1.0 : 1.0;
		double aMagicValue = 16; //@Param
		double velocity = diskMass * impulseSize * aMagicValue; 
		return  (sign * (getDetRand(0.0, sizeOfEnvironmentX, sizeOfEnvironmentY).x / 100))*velocity;
	}

	private static Point getDetRand(double radius, double sizex, double sizey) {
		double xpos = 0.0;
		double ypos = 0.0;

		for(int i=0; i<20; i++) {
			rand.nextDouble();
		}

		boolean within = false;
		do {
			xpos = rand.nextDouble() * sizex;
			ypos = rand.nextDouble() * sizey;
			within = xpos - radius > 0.0 && xpos + radius < sizex && ypos - radius > 0.0 && ypos + radius < sizey;
		} while (!within);
		return new Point (xpos, ypos); 
	}



	/**
	 * returns a position for the "numCurrentDisk" in a perfect circle 
	 * of n=numDisks with a given radius
	 * @param radius
	 * @param numDisks
	 * @param numCurrentDisk
	 * @return
	 */
	private static Point getNthSqrtPos(double radius, int numDisks, int numCurrentDisk, double environmentSize) {
		//		http://de.wikipedia.org/wiki/Einheitswurzel
		//		x_k = \cos (2\pi k/n) = \cos (360^\circ \cdot k/ n )  
		//		und   y_k = \sin (2\pi k/n) = \sin (360^\circ \cdot k/ n ) .

		double x = Math.cos(2 * Math.PI * numCurrentDisk / numDisks) * (environmentSize / 3) + (environmentSize / 2);
		double y = Math.sin(2 * Math.PI * numCurrentDisk / numDisks) * (environmentSize / 3) + (environmentSize / 2);

		return new Point(x,y);
	}

}
