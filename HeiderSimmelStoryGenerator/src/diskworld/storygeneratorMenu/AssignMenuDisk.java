package diskworld.storygeneratorMenu;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AssignMenuDisk extends JFrame {

	protected JButton buttonOK;
	protected JButton assignDiskColorButton;
	protected boolean colorChosen = false;
	
	protected String diskName;
	protected double x;
	protected double y;
	protected double radius;
	protected Color col;
	
	protected JTextField tf1;
	protected JFormattedTextField tf2;
	protected JFormattedTextField tf3;
	protected JFormattedTextField tf4;
	
	public AssignMenuDisk() {
		x = 0.0;
		y = 0.0;
	}
	
	/**
	 * changing a MenuDisk
	 * @param d
	 */
	public AssignMenuDisk(MenuDisk d) {
		setTitle("Changing a disk");
		setSize(600, 100);
		JPanel panel = new JPanel();

		tf1 = new JTextField(d.diskName, 15);
		tf2 = new JFormattedTextField(new DecimalFormat("00"));
		tf2.setColumns(4);
		tf2.setText(Integer.toString((int)(d.x*100.0)));
		tf3 = new JFormattedTextField(new DecimalFormat("00"));
		tf3.setColumns(4);
		tf3.setText(Integer.toString((int)(d.y*100.0)));
		tf4 = new JFormattedTextField(new DecimalFormat("00"));
		tf4.setColumns(4);
		tf4.setText(Integer.toString((int)(d.radius*100.0)));

		designDefaultTextFieldsAndLabel(panel);

		col = d.col;
		designColorChooseButton(panel);

		add(panel);
		getRootPane().setDefaultButton(buttonOK);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}
	
	/**
	 * creating a new MenuDisk
	 * @param listAss
	 */
	public AssignMenuDisk(LinkedList<MenuAssignment> listAss) {
		setTitle("Creating a disk");
		setSize(600, 100);
		JPanel panel = new JPanel();
		
		String diskName = "DiskName"+Integer.toString(listAss.size()+1);
		tf1 = new JTextField(diskName, 15);
		tf2 = new JFormattedTextField(new DecimalFormat("00"));
		tf2.setColumns(4);
		tf2.setText("50");
		x = parse(tf2, 0);
		tf3 = new JFormattedTextField(new DecimalFormat("00"));
		tf3.setColumns(4);
		tf3.setText("50");
		y = parse(tf3, 0);
		tf4 = new JFormattedTextField(new DecimalFormat("00"));
		tf4.setColumns(4);
		tf4.setText("2");
		radius = parse(tf4, 0);

		tf2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				x = parse(tf2,x);
			}
		});
		tf2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				y = parse(tf3,y);
			}
		});
		tf3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				radius = parse(tf3,radius);
			}
		});

		designDefaultTextFieldsAndLabel(panel);

		col = A_MenuMain.DEFAULT_DISK_COLOR;
		
		designColorChooseButton(panel);

		add(panel);
		getRootPane().setDefaultButton(buttonOK);
		setLocationRelativeTo(null);
		setResizable(false);
		setAlwaysOnTop(true);
		setVisible(true);
	}

	private double parse(JFormattedTextField tf, double fallBack) {
		try {
			return Double.parseDouble(tf.getText())/100.0;
		} catch (NumberFormatException e) {
			return fallBack;
		}
	}

	private void designColorChooseButton(JPanel panel) {
		buttonOK = new JButton("OK");
		assignDiskColorButton = new JButton("Color");
		getRootPane().setDefaultButton(buttonOK); 

		panel.add(buttonOK);
		panel.add(assignDiskColorButton);
		
		
		assignDiskColorButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed (ActionEvent e)
			{
				if(e.getSource() == assignDiskColorButton) {
					col = JColorChooser.showDialog(null, "Select Color", Color.BLUE);
					colorChosen = true;
				}
			}
		});
	}

	private void designDefaultTextFieldsAndLabel(JPanel panel) {
		tf1.setForeground(Color.BLUE);
		tf1.setBackground(Color.YELLOW);
		panel.add(tf1);

		tf2.setForeground(Color.BLUE);
		tf2.setBackground(Color.YELLOW);
		panel.add(tf2);

		tf3.setForeground(Color.BLUE);
		tf3.setBackground(Color.YELLOW);
		panel.add(tf3);

		tf4.setForeground(Color.BLUE);
		tf4.setBackground(Color.YELLOW);
		panel.add(tf4);

		JLabel jlabel1 = new JLabel("Please enter x, y and radius e[0, 1] without `.�  e.g. 13 instead of 0.13");
		panel.add(jlabel1);
	}
	
	public boolean isUsedDiskName(LinkedList<MenuAssignment> listAss) {
		boolean isDisknameAlreadyUsed = false;
		for(MenuAssignment currAssignment : listAss) {
			if(currAssignment.thisDisk.diskName.equals(this.diskName)) {
				isDisknameAlreadyUsed = true;
			}
		}
		return isDisknameAlreadyUsed;
	}

	public void setCircleData(double x, double y, double r) {
		tf2.setText(""+(int)Math.round(100*x));
		tf3.setText(""+(int)Math.round(100*y));
		tf4.setText(""+(int)Math.round(100*r));
		this.x = x;
		this.y = y;
		this.radius = r;
	}


	
}
