package diskworld.storygeneratorMenu;

/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 


//@R
//copied from:	http://docs.oracle.com/javase/tutorial/uiswing/examples/components/TableRenderDemoProject/src/components/TableRenderDemo.java

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import diskworld.storygenerator.Behaviour;
import diskworld.storygenerator.Events;
import diskworld.storygenerator.Interactions;

public class BigTable extends JPanel {

	private static final boolean DEBUG = false;
	private static int length; //num of ias that are passed on to ChooseEvents plus one
	private MenuAssignment currAssPointer;
	private LinkedList<MenuAssignment> listAssPointer;
	private ChooseEvents chooseEventsPointer;
	private LeftPaintPanel paintPanel;
	private A_MenuMain mainMenuPointer;
	JSplitPane splitpane;
	private JTable table;

	private static String changeString = "Change";
	private static String deleteString = "Delete";

	public BigTable(final LinkedList<MenuAssignment> listAss,
			final MenuAssignment currAss,
			final ChooseEvents chooseEvents,
			LeftPaintPanel paintPanel,
			JSplitPane splitpane,
			A_MenuMain mainMenu) {
		
		super(new GridLayout(1,0));

		currAssPointer = currAss;
		listAssPointer = listAss;
		chooseEventsPointer = chooseEvents;
		mainMenuPointer = mainMenu;
		this.paintPanel = paintPanel;
		this.splitpane = splitpane;

		this.setBorder( BorderFactory.createLineBorder( Color.black ) );

		if(listAss.size()<1) { //less than 1 disk makes no sense
			System.out.println("Error at BigTable");
		} else if(currAssPointer.matrix.length == 0) {
//			System.out.println("BigTable: length too short!");
		} else {

			length = currAssPointer.listInteractions.size()+1;

			String[] events;
			if(listAss.size() <= 1) { //only 1 disk
				events = Events.getEventNamesWithoutRefDiskNeeded();
			} else {
				events = Events.getAllEventNames();
			}

			final String[] eventNamesList = new String[events.length+1];

			eventNamesList[0] = A_MenuMain.DEFAULT_STRING;
			for(int i=1; i<eventNamesList.length; i++) {
				eventNamesList[i] = events[i-1];
			}

			table = new JTable(new MyTableModel());
			table.setPreferredScrollableViewportSize(new Dimension(700, 300));
			table.setFillsViewportHeight(true);

			//Create the scroll pane and add the table to it.
			JScrollPane scrollPane = new JScrollPane(table);

			//Set up column sizes.
			initColumnSizes(table);

			setUpInteractionsCol(table, table.getColumnModel().getColumn(0));

			//Fiddle with the selection column's cell editors/renderers.	
			for(int i=1; i<=currAssPointer.listInteractions.size(); i++) {
				setUpEventsCol(table, table.getColumnModel().getColumn(i), eventNamesList);
			}

			//Add the scroll pane to this panel.
			this.add(scrollPane);	
		}
	}

	/**
	 * This method picks good column sizes.
	 * If all column heads are wider than the column's cells'
	 * contents, then you can just use column.sizeWidthToFit().
	 */
	private void initColumnSizes(JTable table) {
		MyTableModel model = (MyTableModel)table.getModel();
		TableColumn column = null;
		Component comp = null;
		int headerWidth = 0;
		int cellWidth = 0;
		Object[] longValues = model.longValues;
		TableCellRenderer headerRenderer =
				table.getTableHeader().getDefaultRenderer();

		length = currAssPointer.listInteractions.size()+1;

		for (int i = 0; i < length; i++) {
			column = table.getColumnModel().getColumn(i);

			comp = headerRenderer.getTableCellRendererComponent(
					null, column.getHeaderValue(),
					false, false, 0, 0);
			headerWidth = comp.getPreferredSize().width;

			comp = table.getDefaultRenderer(model.getColumnClass(i)). //here
					getTableCellRendererComponent(
							table, longValues[i],
							false, false, 0, i);
			cellWidth = comp.getPreferredSize().width;

			if (DEBUG) {
				System.out.println("Initializing width of column "
						+ i + ". "
						+ "headerWidth = " + headerWidth
						+ "; cellWidth = " + cellWidth);
			}

			column.setPreferredWidth(Math.max(headerWidth, cellWidth));
		}
	}

	public void setUpInteractionsCol(JTable table, TableColumn selectionCol) {

		String[] possibleActionsList = {changeString, deleteString};

		@SuppressWarnings({ "rawtypes", "unchecked" })
		final JComboBox comboBox = new JComboBox(possibleActionsList);

		selectionCol.setCellEditor(new DefaultCellEditor(comboBox));

		//Set up tool tips for the selection cells.
		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
		renderer.setToolTipText("Click for changing or deleting this Interaction");
		selectionCol.setCellRenderer(renderer);
	}

	public void setUpEventsCol(JTable table, TableColumn selectionCol,
			String[] eventNamesList) {

		@SuppressWarnings({ "rawtypes", "unchecked" })
		final JComboBox comboBox = new JComboBox(eventNamesList);

		selectionCol.setCellEditor(new DefaultCellEditor(comboBox));

		//Set up tool tips for the selection cells.
		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
		renderer.setToolTipText("Click for assigning Events");
		selectionCol.setCellRenderer(renderer);
	}


	class MyTableModel extends AbstractTableModel {

		Object[][] data = getData();

		private final Object[][] getData() {
			Object[][]data = new Object[length-1][length];
			for(int i=0; i<data.length; i++) {
				for(int j=0; j<data[i].length; j++) {
					if(j==0) {
						data[i][j] = this.getColumnName(i+1); //write IA names in 1st col
					} else {
						if(A_MenuMain.DEFAULT_STRING.equals(currAssPointer.matrix[i][j-1].getName())) {
							data[i][j] = A_MenuMain.DEFAULT_STRING;
						} else {
							data[i][j] = currAssPointer.matrix[i][j-1].getName();
						}
					}
				}
			}
			return data;
		}


		public final Object[] longValues = getLongValues();

		private final Object[] getLongValues() {
			Object[]longValues = new Object[length];
			for(int i=0; i<longValues.length; i++) {
				longValues[i] = A_MenuMain.DEFAULT_STRING;
			}
			return longValues;
		}

		public int getColumnCount() {
			return length;
		}

		public int getRowCount() {
			return length-1;
		}

		public String getColumnName(int col) {
			if(col==0) {
				return "--Interactions--";
			}
			return currAssPointer.listInteractions.get(col-1).getName();
		}

		public Object getValueAt(int row, int col) {
			if(row > (length-1) || col > length) {
				throw new IllegalArgumentException("Error at getValueAt: "
						+ "wrong row/col size");
			} else {
//				System.out.println("test- row: "+row+", col: "+col+", length: "+length+", data: "+data[row][col]);
				return data[row][col];
			}
		}

		public Class<? extends Object> getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}

		public boolean isCellEditable(int row, int col) {
			return true;
		}

		public void setValueAt(Object value, final int row, final int col) {

			String eventOrInteractionName = value.toString();

			if(col==0) {				
				if(eventOrInteractionName.equals(deleteString)) {
					if(currAssPointer.listInteractions.size()<=1) {
						new ShowErrorMessage("last interaction",
								"You can�t delete the last interaction."
										+ " If you want to delete this disk,"
										+ " choose `Delete a disk�.");
					} else {
						final ShowOKSureMessage okSureWindow = new ShowOKSureMessage("Sure?", "If you delete this"
								+ " Interaction, every Event that would leed"
								+ " to this Interaction will be deleted as well.");

						okSureWindow.buttonOK.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed (ActionEvent e)
							{
								if(e.getSource() == okSureWindow.buttonOK) {
									okSureWindow.isChosen = true;
									okSureWindow.frame.setVisible(false);

									chooseEventsPointer.del(row, listAssPointer, currAssPointer);
									
//									chooseEventsPointer.deleteInteractionHelper(row, listAssPointer, currAssPointer);
//									currAssPointer.listInteractions.remove(row);
//									chooseEventsPointer.updateTable(listAssPointer, currAssPointer);
									
								}
							}
						});

					}
				} else if(eventOrInteractionName.equals(changeString)) {
					//change an existing Interaction

					final Behaviour previousInteraction = currAssPointer.listInteractions.get(row);
//					System.out.println("test: numIA�s: "+currAssPointer.listInteractions.size());

//					System.out.println("test 0: "+previousInteraction.getName());
					final BehaviourChooser interactionChooser = new BehaviourChooser(listAssPointer,
							currAssPointer, previousInteraction,
							previousInteraction.getName(), false, paintPanel,
							splitpane, mainMenuPointer);

					interactionChooser.buttonOK.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed (ActionEvent e)
						{
							if(e.getSource() == interactionChooser.buttonOK) {

								Interactions newIA = interactionChooser.getSelectedInteraction();

								if(interactionChooser.isChosen) {

									currAssPointer.listInteractions.set(row, newIA);

									String nameOfNewSelectedInteraction = newIA.getName();
									System.out.println("now IA changed: "+nameOfNewSelectedInteraction+""
											+ " replaces "+previousInteraction.getName());

									fireTableCellUpdated(row, col);
									data[row][col] = nameOfNewSelectedInteraction;
									
									//updating column head as well
									table.getTableHeader().getColumnModel().getColumn(row+1).setHeaderValue(nameOfNewSelectedInteraction);
									repaint();
									
								}
							}
						}
					});					

				} else {
					throw new IllegalArgumentException("Object 'value' unknown");
				}

			} else {

				if(! (eventOrInteractionName.equals(A_MenuMain.DEFAULT_STRING))) {

					Events previousEvent = currAssPointer.matrix[row][col-1];

					final BehaviourChooser eventChooser;

					if((previousEvent.getName().equals(A_MenuMain.DEFAULT_STRING))) {
						eventChooser = new BehaviourChooser(listAssPointer, currAssPointer, null, eventOrInteractionName, true, paintPanel, splitpane, mainMenuPointer);
					} else {
						eventChooser = new BehaviourChooser(listAssPointer, currAssPointer, previousEvent, eventOrInteractionName, true, paintPanel, splitpane, mainMenuPointer);
					}
					eventChooser.buttonOK.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed (ActionEvent e)
						{
							if(e.getSource() == eventChooser.buttonOK) {

								Events newEvent = eventChooser.getSelectedEvent();

								if(eventChooser.isChosen) {
									addEvent(row, col, newEvent);
								}
							}
						}			
					});
				} else {
					// default String is chosen -> delete event
					if(!(data[row][col].equals(A_MenuMain.DEFAULT_STRING))) {
						deleteEvent(row, col);
					}
				}
			}
		}

		private void addEvent(final int row, final int col,
				Behaviour newEvent) {

			System.out.println("now new Event added: "+newEvent.getName());

			changeEvent(row, col, newEvent, false);
		}
		
		private void deleteEvent(final int row, final int col) {
			System.out.println("now Event deleted: "+currAssPointer.matrix[row][col-1].getName());
			changeEvent(row, col, new Events(), true);
		}

		private void changeEvent(final int row, final int col, Behaviour newEvent,
				boolean isDefaultEvent) {
			currAssPointer.matrix[row][col-1] = (Events) newEvent;
			if(isDefaultEvent) {
				data[row][col] = A_MenuMain.DEFAULT_STRING;
			} else {
				data[row][col] = newEvent.getName();
			}
			
			fireTableCellUpdated(row, col);

			if (DEBUG) {
				System.out.println("Setting value at " + row + "," + col
						+ " to " + newEvent.getName());

				System.out.println("New value of data:");
				printDebugData();
			}
		}

		private void printDebugData() {
			int numRows = getRowCount();
			int numCols = getColumnCount();

			for (int i=0; i < numRows; i++) {
				System.out.print("    row " + i + ":");
				for (int j=1; j < numCols; j++) {
					System.out.print("  " + data[i][j]);
				}
				System.out.println();
			}
			System.out.println("--------------------------");
		}
	}
}

