package diskworld.storygeneratorMenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;

import diskworld.storygenerator.Behaviour;
import diskworld.storygenerator.Events;
import diskworld.storygenerator.Interactions;

public class ChooseEvents extends ChooseMenu {

	private BigTable table;
	private JButton addIAButton 	= 	new JButton(" add an Interaction  ");

	private LinkedList<MenuAssignment> listAss;
	private MenuAssignment currAss;
	private LeftPaintPanel paintPanel;
	private JSplitPane splitpane;
	private A_MenuMain mainMenu;

	public ChooseEvents(final LinkedList<MenuAssignment> listAssignments,
			final MenuAssignment currAssignment, LeftPaintPanel paintPanel,
			JSplitPane splitpane, A_MenuMain mainMenu) {

		this.listAss = listAssignments;
		this.currAss = currAssignment;
		this.paintPanel = paintPanel;
		this.splitpane = splitpane;
		this.mainMenu = mainMenu;

		setBackground(Color.white);

		final JLabel label1 = new JLabel("Please add an Interaction for this disk.");
		add(label1);
		setBorder( BorderFactory.createLineBorder( Color.black ) );
		this.setBackground(Color.white);

		if(currAss.listInteractions.size()>0) {
			this.perform(listAss, currAss);
		} else {

			final JButton addFirstIAButton = new JButton(" add an Interaction  ");
			add(addFirstIAButton);

			addFirstIAButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed (ActionEvent e)
				{
					if(e.getSource() == addFirstIAButton) {						
						remove(label1);
						remove(addFirstIAButton);

						selectAnInteraction(listAss, currAss, true);
					}
				}
			});
		}
	}


	private void perform(final LinkedList<MenuAssignment> listAss, final MenuAssignment currAss) {
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new GridLayout(1,2));
		northPanel.add(addIAButton);

		setLayout(new BorderLayout());
		add(northPanel, BorderLayout.NORTH);

		updateTable(listAss, currAss);

		addIAButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed (ActionEvent e)
			{
				if(e.getSource() == addIAButton) {
					selectAnInteraction(listAss, currAss, false);
				}
			}
		});
	}


	private void selectAnInteraction(final LinkedList<MenuAssignment> listAss,
			final MenuAssignment currAss, final boolean isFirstInteraction) {

		final BehaviourChooser interactionChooser = 
				new BehaviourChooser(listAss, currAss, null, A_MenuMain.DEFAULT_STRING,
						false, this.paintPanel, splitpane, mainMenu);

		interactionChooser.buttonOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed (ActionEvent e)
			{
				if(e.getSource() == interactionChooser.buttonOK) {

					Interactions newIA = interactionChooser.getSelectedInteraction();

					if(interactionChooser.isChosen) {
						currAss.listInteractions.add(newIA);

						String nameOfNewSelectedInteraction = newIA.getName();
						System.out.println("now new IA added: "+nameOfNewSelectedInteraction);
						if(isFirstInteraction) {
							perform(listAss, currAss);
						} else {
							updateTable(listAss, currAss);
						}
					}
				}
			}
		});
	}

	/**
	 * auxiliary method, to be called after a user change in the table.
	 * @param listAss
	 * @param currAss
	 */
	protected void updateTable(final LinkedList<MenuAssignment> listAss,
			final MenuAssignment currAss) {

		currAss.matrix = getNewMatrix(currAss);

		if(table!=null) {
			remove(table);
			table=null;
		}
		table = new BigTable(listAss, currAss, this, paintPanel, splitpane, mainMenu);
		add(table, BorderLayout.CENTER);
		validate();
	}

	protected void deleteBehaviourWithReferenceDisk(String nameOfDiskToDelete) {
		for(MenuAssignment ass : listAss) {
			deleteIA(nameOfDiskToDelete, listAss, ass);
			deleteEvent(nameOfDiskToDelete, ass);
		}
	}

	private void deleteIA(String nameOfDiskToDelete, 
			LinkedList<MenuAssignment> listAss, MenuAssignment ass) {

		for(int i=ass.listInteractions.size()-1; i>=0; i--) {
			Behaviour currIA = ass.listInteractions.get(i);
			if(currIA.referenceDiskNeeded()
					&& currIA.getMenuReferenceDiskName().equals(nameOfDiskToDelete)) {
				//				System.out.println("delete "+currIA.getName());
				ass.listInteractions.remove(i);
				deleteInteractionHelper(i, listAss, ass);
			}
		}
	}


	private void deleteEvent(String nameOfDiskToDelete, MenuAssignment ass) {
		for(int i=0; i<ass.matrix.length; i++) {
			for(int j=0; j<ass.matrix[i].length; j++) {
				if(ass.matrix[i][j].referenceDiskNeeded() 
						&& ass.matrix[i][j].getMenuReferenceDiskName().equals(nameOfDiskToDelete)) {
					ass.matrix[i][j] = new Events();
					updateTable(listAss, ass);
				}
			}
		}
	}
	
	protected void del(int row, LinkedList<MenuAssignment>listAssPointer,
			MenuAssignment currAssPointer) {
		deleteInteractionHelper(row, listAssPointer, currAssPointer);
		currAssPointer.listInteractions.remove(row);
		updateTable(listAssPointer, currAssPointer);
	}

	protected void deleteInteractionHelper(int interactionToDeleteNum,
			LinkedList<MenuAssignment> listAss,
			MenuAssignment currAss) {

		int oldMatrixLength = currAss.matrix.length;

		Events[][]newMatrix = new Events[oldMatrixLength-1][oldMatrixLength-1];
		for(int i=0; i<oldMatrixLength-1; i++) {
			for(int j=0; j<oldMatrixLength-1; j++) {
				if(i<interactionToDeleteNum && j<interactionToDeleteNum) {
					newMatrix[i][j] = currAss.matrix[i][j];
				} else if(i<interactionToDeleteNum && j>=interactionToDeleteNum) {
					newMatrix[i][j] = currAss.matrix[i][j+1];
				} else if(i>=interactionToDeleteNum && j<interactionToDeleteNum) {
					newMatrix[i][j] = currAss.matrix[i+1][j];
				} else {
					newMatrix[i][j] = currAss.matrix[i+1][j+1];
				}
			}
		}
		currAss.matrix = newMatrix;
		updateTable(listAss, currAss);
	}
	

	/**
	 * returns a copy of currAss.matrix with the difference that
	 * if currAss.interactions is larger than before, the events matrix
	 * will be larger as well, filled with default Events in the respective
	 * rows and columns.
	 * @param currAss
	 * @return
	 */
	private Events[][]getNewMatrix(MenuAssignment currAss) {
		int size = currAss.listInteractions.size();

		Events[][] newMatrix = new Events[size][size];
		for(int i=0; i<size; i++) {
			for(int j=0; j<size; j++) {
				if(i < currAss.matrix.length && j<currAss.matrix.length) {
					newMatrix[i][j] = currAss.matrix[i][j];
				} else {
					newMatrix[i][j] = new Events();
				}
			}
		}
		return newMatrix;
	}
}

