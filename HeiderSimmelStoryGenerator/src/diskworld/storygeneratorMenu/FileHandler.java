package diskworld.storygeneratorMenu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.LinkedList;

import javax.swing.JFileChooser;

import diskworld.linalg2D.Line;
import diskworld.storygenerator.Story;

/**
 * various classes to handle choosing / saving / reading from files.
 */
public class FileHandler {
	
	protected static boolean isStory(File file) {
		InputStream fis = null;
		Story sampleStory = null;
		try {
			fis = new FileInputStream( file );
			ObjectInputStream o = new ObjectInputStream( fis );

			sampleStory = (Story) o.readObject();
			sampleStory.addValuesToEventsMatrix();
		} catch ( IOException e ) {
			System.out.println(e);
			return false;
		} catch ( ClassNotFoundException e ) {
			System.out.println(e);
			return false;
		}

		finally {
			try {
				fis.close(); 
			} catch ( Exception e ) {
				System.out.println("Close failed: "+e);
				return false;
			} 
		}

		return true;
	}

	protected static Story getStoryFromFile(File file) {
		InputStream fis = null;
		Story sampleStory = null;
		try
		{
			fis = new FileInputStream( file );
			ObjectInputStream o = new ObjectInputStream( fis );

			sampleStory = (Story) o.readObject();
			sampleStory.addValuesToEventsMatrix();
		}
		catch ( IOException e ) {
			System.out.println(e);
		}
		catch ( ClassNotFoundException e ) {
			System.out.println(e);
		}

		finally {
			try {
				fis.close(); 
			} catch ( Exception e ) {
				System.out.println(e);
			} 
		}
		return sampleStory;
	}

	protected static void openFileSafer(String nameOfFile, Story story) {
		openFileSafer(nameOfFile, story.getListOfMenuAsssignments(), story.getStoryname(),
				story.getListOfWalls(), story.getShowVisualization());
	}

	protected static void openFileSafer(String nameOfFile,
			LinkedList<MenuAssignment> listAss, 
			String storyName,
			LinkedList<Line> wallList,
			boolean showVisualization) {

		Story myStory = new Story(listAss, storyName, wallList, showVisualization);

		JFileChooser fc = new JFileChooser();

		fc.setSelectedFile( new File( nameOfFile ) );

		int state = fc.showSaveDialog(null);

		if ( state == JFileChooser.APPROVE_OPTION ) {
			File chosenDirectory = fc.getSelectedFile();

			safeStoryToFile(chosenDirectory, myStory);

		} else if(state != JFileChooser.CANCEL_OPTION){
			new ShowErrorMessage("Error - wrong file", "Please choose a "
					+ ".txt file!");
		}
	}

	protected static void safeStoryToFile(File chosenDirectory, Story myStory) {
		OutputStream fos = null;

		System.out.println("file safed in directory: "+chosenDirectory);

		try {

			File file = new File(chosenDirectory+".txt");

			fos = new FileOutputStream( file );

			ObjectOutputStream o = new ObjectOutputStream( fos );

			o.writeObject(myStory);

			fos.flush();
			o.close();
		} catch ( IOException e ) {
			System.err.println( e );
		}

		finally {
			try {
				fos.close();
			} catch ( Exception e ) {
				e.printStackTrace();
			}
		}
	}

}
