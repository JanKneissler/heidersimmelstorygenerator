package diskworld.storygeneratorMenu;

import java.awt.Cursor;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MenuBar extends JMenuBar { 
	
	protected JMenuItem menuNewStory = new JMenuItem("  New story  ");
	protected JMenuItem menuLoadStory = new JMenuItem("  Load a story  ");
	protected JMenuItem menuSafeStory = new JMenuItem("  Safe this story  ");		
	protected JMenuItem menuRunNewSimulation = new JMenuItem("  Run Simulation  ");
	protected JMenuItem menuPauseSimulation = new JMenuItem(" Stop Simulation ");
	protected JMenuItem menuEditStory = new JMenuItem(" Edit Story ");
	
	private boolean isRunning = false;

	MenuBar(){
		this.add(menuNewStory);
		this.add(menuLoadStory);
		this.add(menuSafeStory);
		this.add(menuRunNewSimulation);
		this.add(menuPauseSimulation);
		this.add(menuEditStory);
		
		final int HAND_CURSOR = 12;
		Cursor cursor = new Cursor(HAND_CURSOR);
		this.setCursor(cursor);
	}
}
